loadPackage "NumericalAlgebraicGeometry";
---- Craighero-Gattazzo surface
rring=CC[p];rsols=roots(p^3+p^2-1,Precision=>100);r=rsols_0;
a=r^2;b=-(2*r^2-13*r-18)/7;c=(73*r^2+75*r+92)/49;e=-(r^2-24*r-9)/7;f=(191*r^2+241*r+163)/49;m=(3*r^2+5*r+1)/7;
R=CC[x,y,z,t];q_3=(x+m*y+a*z)^2;q_2=a^2*x^3+x*y*(b*x+c*y)+m^2*y^3+(e*x^2+f*x*y+c*y^2)*z+(b*x+e*y)*z^2+z^3;q_1=2*a*x^3*y+e*x^2*y^2+2*a*m*x*y^3+(2*a*m*x^3+f*x^2*y+f*x*y^2+2*m*y^3)*z+(c*x^2+f*x*y+b*y^2)*z^2+2*(m*x+a*y)*z^3;q_0=x^3*y^2+a^2*x^2*y^3+x*y*(2*m*x^2+b*x*y+2*a*y^2)*z+(m^2*x^3+c*x^2*y+e*x*y^2+y^3)*z^2+(m*x+a*y)^2*z^3;
cgquintic=sum(0..3,i -> q_i*t^i);fermatquintic=x^5+y^5+z^5+t^5;
mu=exp(2*pi*ii/5);ijk = (nbr) -> (nbr%5,(nbr//5)%5,nbr//25);
nstep=19;

-- Work in affine charts
affineopen0=CC[X,Y,Z];affinecoords0=map(affineopen0,R,{x=>X,y=>Y,z=>Z,t=>1}); -- xyz -- function is y/xz = Y/XZ
F0=affinecoords0(fermatquintic);Q0=affinecoords0(ii*cgquintic);
htpymethod0 = (paramt,ce0,cp0) -> (
    S=paramt*Q0+(1-paramt)*F0;
    Sx=diff(X,S);Sy=diff(Y,S);Sz=diff(Z,S);finaleqs={X*Sx+Y*Sy,Y*Sy+Z*Sz,S}; -- xyz
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
F0x=diff(X,F0);F0y=diff(Y,F0);F0z=diff(Z,F0);initeqs0={X*F0x+Y*F0y,Y*F0y+Z*F0z,F0}; -- xyz
muijk0 = (i,j,k) -> (-mu^i,mu^j,-mu^k); -- xyz
initpts0=toList(apply(0..124,nbr -> muijk0(ijk(nbr))));
critpts = apply(0..nstep,i -> htpymethod0(i/nstep,initeqs0,initpts0));
-- critpts is a list with nstep+1 entries
-- each entry is a list of 125 triples
pencil = pt -> (coordinates pt)_1/((coordinates pt)_2*(coordinates pt)_0);
critvals=for i from 0 to nstep list apply(critpts_i,pencil);
critvals_0
critvals_1
critvals_2
critvals_3
critvals_4
critvals_5
critvals_6
critvals_7
critvals_8
critvals_9
critvals_(10)
critvals_(11)
critvals_(12)
critvals_(13)
critvals_(14)
critvals_(15)
critvals_(16)
critvals_(17)
critvals_(18)
critvals_(19)
subset0={3, 7, 28, 29, 33, 37, 46, 54, 72, 90, 93, 99, 107, 113, 116, 123, 124};
indic0 = i -> if member(i,subset0) then true else false;
subset1={5, 9, 10, 19, 42, 48, 49, 57, 60, 70, 75, 76, 83, 86, 101, 102, 105};
indic1 = i -> if member(i,subset1) then true else false;
subset2={18, 25, 36, 41, 45, 50, 56, 59, 62, 67, 68, 77, 89, 94, 95, 103, 117};
indic2 = i -> if member(i,subset2) then true else false;
subset3={1, 2, 12, 14, 15, 21, 32, 34, 35, 53, 66, 79, 88, 96, 111, 114, 119};
indic3 = i -> if member(i,subset3) then true else false;
subset4={0, 4, 6, 8, 11, 13, 16, 17, 20, 22, 23, 24, 26, 27, 30, 31, 38, 39, 40, 43, 44, 47, 51, 52, 55, 58, 61, 63, 64, 65, 69, 71, 73, 74, 78, 80, 81, 82, 84, 85, 87, 91, 92, 97, 98, 100, 104, 106, 108, 109, 110, 112, 115, 118, 120, 121, 122};
indic4 = i -> if member(i,subset4) then true else false;

critpts0=for i from 0 to nstep list select(critpts_i,indic0);
critpts0
critpts1=for i from 0 to nstep list select(critpts_i,indic0);
critpts2=for i from 0 to nstep list select(critpts_i,indic0);
critpts3=for i from 0 to nstep list select(critpts_i,indic0);
critpts4=for i from 0 to nstep list select(critpts_i,indic0);

pencil = pt -> (coordinates pt)_1/((coordinates pt)_2*(coordinates pt)_0);

critvals0=for i from 0 to nstep list apply(critpts0_i,pencil)
critvals1=for i from 0 to nstep list apply(critpts1_i,pencil);
critvals2=for i from 0 to nstep list apply(critpts2_i,pencil);
critvals3=for i from 0 to nstep list apply(critpts3_i,pencil);
critvals4=for i from 0 to nstep list apply(critpts4_i,pencil);
