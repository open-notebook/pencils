loadPackage "NumericalAlgebraicGeometry";
---- Craighero-Gattazzo surface
rring=CC[p];
rsols=roots(p^3+p^2-1,Precision=>100);
r=rsols_0;
a=r^2;
b=-(2*r^2-13*r-18)/7;
c=(73*r^2+75*r+92)/49;
e=-(r^2-24*r-9)/7;
f=(191*r^2+241*r+163)/49;
m=(3*r^2+5*r+1)/7;
R=CC[x,y,z,t];
q_3=(x+m*y+a*z)^2;
q_2=a^2*x^3+x*y*(b*x+c*y)+m^2*y^3+(e*x^2+f*x*y+c*y^2)*z+(b*x+e*y)*z^2+z^3;
q_1=2*a*x^3*y+e*x^2*y^2+2*a*m*x*y^3+(2*a*m*x^3+f*x^2*y+f*x*y^2+2*m*y^3)*z+(c*x^2+f*x*y+b*y^2)*z^2+2*(m*x+a*y)*z^3;
q_0=x^3*y^2+a^2*x^2*y^3+x*y*(2*m*x^2+b*x*y+2*a*y^2)*z+(m^2*x^3+c*x^2*y+e*x*y^2+y^3)*z^2+(m*x+a*y)^2*z^3;
cgquintic=sum(0..3,i -> q_i*t^i);
fermatquintic=x^5+y^5+z^5+t^5;
--automorphism=map(R,R,{x=>y,y=>z,z=>t,t=>x});
--cgimage=automorphism(cgquintic);
--diffce=cgimage-cgquintic
-- Work in an affine chart
affineopen=CC[X,Y,Z];
affinecoords=map(affineopen,R,{x=>X,y=>1,z=>Z,t=>Y});
F=affinecoords(fermatquintic);
Q=affinecoords(ii*cgquintic);
-- The pencil is given by the rational function p(x,y,z)=y*z/x in this
-- affine chart.  At time t in the deformation, we are interested in
-- the quintic S=tQ+(1-t)F. We need to find the critical points of
-- p - l S, where l is a Lagrange multiplier.
-- We have dp/dx=-yz/x^2, dp/dy=z/x, dp/dz=y/x
-- so the critical point equations are
--  0        =    S
-- -yz / x^2 = l dS/dx
--  z  / x   = l dS/dy
--  y  / x   = l dS/dz
-- We can eliminate the Lagrange multiplier to get
-- x dS/dx = - y dS/dy = -z dS/dz, S=0
-- For the charts where y=1 or z=1, we use
-- yS_y+xS_x=0, yS_y+zS_z=0 and xS_x+zS_z=0
htpymethod = (paramt,ce0,cp0) -> (
    S=paramt*Q+(1-paramt)*F;
    Sx=diff(X,S);
    Sy=diff(Y,S);
    Sz=diff(Z,S);
    finaleqs={X*Sx+Y*Sy,Y*Sy+Z*Sz,S};
    --finaleqs={X*Sx+Y*Sy,X*Sx+Z*Sz,S};
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
-- We use the homotopy method to track the critical points from 0 to
-- t, so we need to know:
-- the critical equations at time 0
Fx=diff(X,F);
Fy=diff(Y,F);
Fz=diff(Z,F);
initeqs={X*Fx+Y*Fy,X*Fx+Z*Fz,F};
-- the critical points at time 0:
-- F = 1+x^5+y^5+z^5
-- xFx+yFy = 5x^5+5y^5
-- xFx+zFz = 5x^5+5z^5
-- so 1+z^5=0 and z=5th root of -1
--    1+y^5=0 and y=5th root of -1
--    x^5=-y^5=1 and x is a 5th root of 1
mu=exp(2*pi*ii/5);
ijk = (nbr) -> (nbr%5,(nbr//5)%5,nbr//25);
muijk = (i,j,k) -> (mu^i*mu^(3*k)*mu^j,-mu^i*mu^(2*k),-mu^k);
initpts=toList(apply(0..124,nbr -> muijk(ijk(nbr))));
nstep=20;
--tabl2=flatten(table({0,1,2,3,4},{0,1,2,3,4},(i,j)->(mu^i,-mu^j)));
--initpts=flatten(table(tabl2,{0,1,2,3,4},(prs,k) -> join(prs,toSequence {-mu^k})));
--critpts = apply(0..nstep,i -> htpymethod(i/nstep,initeqs,initpts));
critpts=htpymethod(1,initeqs,initpts);
--getsmall = i -> (if sqrt(abs((coordinates i)_0)^2+abs((coordinates i)_1)^2+abs((coordinates i)_2)^2)<0.00001 then true else false);
--ans=#select(critpts,getsmall)
--godeaux_critpts = apply(0..nstep,i -> toList(apply(0..124,j -> critpts_i_j)));
--pencil = pt -> ((coordinates pt)_1*(coordinates pt)_2/(coordinates pt)_0);
--critvals=toList(apply(critpts,pencil));
actualpoints=toList(apply(0..124,nbr -> coordinates(critpts_nbr)))
pointstatuses=toList(apply(0..124,nbr -> status(critpts_nbr)))
number(pointstatuses, i -> i== MinStepFailure)