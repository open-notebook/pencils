loadPackage "NumericalAlgebraicGeometry";
---- Craighero-Gattazzo surface
rring=CC[p];rsols=roots(p^3+p^2-1,Precision=>100);r=rsols_0;
a=r^2;b=-(2*r^2-13*r-18)/7;c=(73*r^2+75*r+92)/49;e=-(r^2-24*r-9)/7;f=(191*r^2+241*r+163)/49;m=(3*r^2+5*r+1)/7;
R=CC[x,y,z,t];q_3=(x+m*y+a*z)^2;q_2=a^2*x^3+x*y*(b*x+c*y)+m^2*y^3+(e*x^2+f*x*y+c*y^2)*z+(b*x+e*y)*z^2+z^3;q_1=2*a*x^3*y+e*x^2*y^2+2*a*m*x*y^3+(2*a*m*x^3+f*x^2*y+f*x*y^2+2*m*y^3)*z+(c*x^2+f*x*y+b*y^2)*z^2+2*(m*x+a*y)*z^3;q_0=x^3*y^2+a^2*x^2*y^3+x*y*(2*m*x^2+b*x*y+2*a*y^2)*z+(m^2*x^3+c*x^2*y+e*x*y^2+y^3)*z^2+(m*x+a*y)^2*z^3;
cgquintic=sum(0..3,i -> q_i*t^i);fermatquintic=x^5+y^5+z^5+t^5;
mu=exp(2*pi*ii/5);ijk = (nbr) -> (nbr%5,(nbr//5)%5,nbr//25);
nstep=19;

-- Work in affine charts
affineopen0=CC[X,Y,Z];affinecoords0=map(affineopen0,R,{x=>X,y=>Y,z=>Z,t=>1}); -- xyz -- function is y/xz = Y/XZ
F0=affinecoords0(fermatquintic);Q0=affinecoords0(ii*cgquintic);
htpymethod0 = (paramt,ce0,cp0) -> (
    S=paramt*Q0+(1-paramt)*F0;
    Sx=diff(X,S);Sy=diff(Y,S);Sz=diff(Z,S);finaleqs={X*Sx+Y*Sy,Y*Sy+Z*Sz,S}; -- xyz
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
F0x=diff(X,F0);F0y=diff(Y,F0);F0z=diff(Z,F0);initeqs0={X*F0x+Y*F0y,Y*F0y+Z*F0z,F0}; -- xyz
muijk0 = (i,j,k) -> (-mu^i,mu^j,-mu^k); -- xyz
initpts0=toList(apply(0..124,nbr -> muijk0(ijk(nbr))));
critpts0=htpymethod0(1,initeqs0,initpts0);

affineopen1=CC[X,Z,T];affinecoords1=map(affineopen1,R,{x=>X,y=>1,z=>Z,t=>T}); -- xzt -- function is t/xz = T/XZ
F1=affinecoords1(fermatquintic);Q1=affinecoords1(ii*cgquintic);
htpymethod1 = (paramt,ce0,cp0) -> (
    S=paramt*Q1+(1-paramt)*F1;
    Sx=diff(X,S);Sz=diff(Z,S);St=diff(T,S);finaleqs={X*Sx+T*St,T*St+Z*Sz,S}; -- xzt
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
F1x=diff(X,F1);F1z=diff(Z,F1);F1t=diff(T,F1);initeqs1={X*F1x+T*F1t,T*F1t+Z*F1z,F1}; -- xzt
muijk1 = (i,j,k) -> (-mu^i/mu^j,-mu^k/mu^j,1/mu^j); -- xzt
initpts1=toList(apply(0..124,nbr -> muijk1(ijk(nbr))));
critpts1=htpymethod1(1,initeqs1,initpts1);

affineopen2=CC[X,Y,T];affinecoords2=map(affineopen2,R,{x=>X,y=>Y,z=>1,t=>T}); -- xyt -- function is yt/x = YT/X
F2=affinecoords2(fermatquintic);Q2=affinecoords2(ii*cgquintic);
htpymethod2 = (paramt,ce0,cp0) -> (
    S=paramt*Q2+(1-paramt)*F2;
    Sx=diff(X,S);Sy=diff(Y,S);St=diff(T,S);finaleqs={X*Sx+T*St,X*Sx+Y*Sy,S}; -- xyt
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
F2x=diff(X,F2);F2y=diff(Y,F2);F2t=diff(T,F2);initeqs2={X*F2x+T*F2t,X*F2x+Y*F2y,F2}; -- xyt
muijk2 = (i,j,k) -> (mu^i/mu^k,-mu^j/mu^k,-1/mu^k); -- xyt
initpts2=toList(apply(0..124,nbr -> muijk2(ijk(nbr))));
critpts2=htpymethod2(1,initeqs2,initpts2);

affineopen3=CC[Y,Z,T];affinecoords3=map(affineopen3,R,{x=>1,y=>Y,z=>Z,t=>T}); -- zyt -- function is yt/z = YT/Z
F3=affinecoords3(fermatquintic);Q3=affinecoords3(ii*cgquintic);
htpymethod3 = (paramt,ce0,cp0) -> (
    S=paramt*Q3+(1-paramt)*F3;
    Sy=diff(Y,S);Sz=diff(Z,S);St=diff(T,S);finaleqs={Z*Sz+T*St,Z*Sz+Y*Sy,S}; -- yzt
    track(ce0,finaleqs,cp0,tStepMin=>0.00000000001));
F3y=diff(Y,F3);F3z=diff(Z,F3);F3t=diff(T,F3);initeqs3={Z*F3z+T*F3t,Z*F3z+Y*F3y,F3}; -- yzt
muijk3 = (i,j,k) -> (-mu^j/mu^i,mu^k/mu^i,-1/mu^i); -- yzt
initpts3=toList(apply(0..124,nbr -> muijk3(ijk(nbr))));
critpts3=htpymethod3(1,initeqs3,initpts3);

getsmall0 = i -> (if sqrt(abs((coordinates critpts0_i)_0)^2+abs((coordinates critpts0_i)_1)^2+abs((coordinates critpts0_i)_2)^2)<0.1 then true else false);getsmall1 = i -> (if sqrt(abs((coordinates critpts1_i)_0)^2+abs((coordinates critpts1_i)_1)^2+abs((coordinates critpts1_i)_2)^2)<0.1 then true else false);getsmall2 = i -> (if sqrt(abs((coordinates critpts2_i)_0)^2+abs((coordinates critpts2_i)_1)^2+abs((coordinates critpts2_i)_2)^2)<0.1 then true else false);getsmall3 = i -> (if sqrt(abs((coordinates critpts3_i)_0)^2+abs((coordinates critpts3_i)_1)^2+abs((coordinates critpts3_i)_2)^2)<0.1 then true else false);
getothers = i -> (if not getsmall0(i) and not getsmall1(i) and not getsmall2(i) and not getsmall3(i) then true else false);
ans0=select(0..124,getsmall0)
ans1=select(0..124,getsmall1)
ans2=select(0..124,getsmall2)
ans3=select(0..124,getsmall3)
ans4=select(0..124,getothers)
toList(apply(ans4,i -> coordinates critpts0_i))