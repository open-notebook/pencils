loadPackage "NumericalAlgebraicGeometry";
R=CC[Y_1,Y_2,Y_3,Y_4];
a_1=1+0.5*ii;a_2=1;a_3=1;
a_4=-1;a_5=1;a_6=1;
nstep=19;
A=matrix{{0,a_1*Y_1,a_2*Y_2,a_2*Y_3,a_1*Y_4},
    {a_1*Y_1,a_3*Y_2,a_4*Y_3,a_5*Y_4,0},
    {a_2*Y_2,a_4*Y_3,a_6*Y_4,0,a_5*Y_1},
    {a_2*Y_3,a_5*Y_4,0,a_6*Y_1,a_4*Y_2},
    {a_1*Y_4,0,a_5*Y_1,a_4*Y_2,a_3*Y_3}};
determinantalquintic=det(A);
fermatquintic=sum(1..4,i -> Y_i^5);
-- Work in an affine chart
affineopen=CC[x,y,z];
affinecoords=map(affineopen,R,{Y_1=>x,Y_2=>y,Y_3=>z,Y_4=>1});
F=affinecoords(fermatquintic);
Q=affinecoords(determinantalquintic);
-- The pencil is given by the rational function p(x,y,z)=y*z/x in this
-- affine chart.  At time t in the deformation, we are interested in
-- the quintic S=tQ+(1-t)F. We need to find the critical points of
-- p - l S, where l is a Lagrange multiplier.
-- We have dp/dx=-yz/x^2, dp/dy=z/x, dp/dz=y/x
-- so the critical point equations are
--  0        =    S
-- -yz / x^2 = l dS/dx
--  z  / x   = l dS/dy
--  y  / x   = l dS/dz
-- We can eliminate the Lagrange multiplier to get
-- x dS/dx = - y dS/dy = -z dS/dz, S=0
htpymethod = (paramt,ce0,cp0) -> (
    S=paramt*Q+(1-paramt)*F;
    Sx=diff(x,S);
    Sy=diff(y,S);
    Sz=diff(z,S);
    finaleqs={x*Sx+y*Sy,x*Sx+z*Sz,S};
    track(ce0,finaleqs,cp0,tStepMin=>0.0000001));
-- We use the homotopy method to track the critical points from 0 to
-- t, so we need to know:
-- the critical equations at time 0
Fx=diff(x,F);
Fy=diff(y,F);
Fz=diff(z,F);
initeqs={x*Fx+y*Fy,x*Fx+z*Fz,F};
-- the critical points at time 0:
-- F = 1+x^5+y^5+z^5
-- xFx+yFy = 5x^5+5y^5
-- xFx+zFz = 5x^5+5z^5
-- so 1+z^5=0 and z=5th root of -1
--    1+y^5=0 and y=5th root of -1
--    x^5=-y^5=1 and x is a 5th root of 1
mu=exp(2*pi*ii/5);
ijk = (nbr) -> (nbr%5,(nbr//5)%5,nbr//25);
muijk = (i,j,k) -> (mu^i*mu^(3*k)*mu^j,-mu^i*mu^(2*k),-mu^k);
initpts=toList(apply(0..124,nbr -> muijk(ijk(nbr))));
--tabl2=flatten(table({0,1,2,3,4},{0,1,2,3,4},(i,j)->(mu^i,-mu^j)));
--initpts=flatten(table(tabl2,{0,1,2,3,4},(prs,k) -> join(prs,toSequence {-mu^k})));
critpts = apply(0..nstep,i -> htpymethod(i/nstep,initeqs,initpts))
godeaux_critpts = apply(0..nstep,i -> toList(apply(0..24,j -> critpts_i_j)))
pencil = pt -> ((coordinates pt)_1*(coordinates pt)_2/(coordinates pt)_0);
critvals= for i from 0 to nstep list apply(godeaux_critpts_i,pencil)