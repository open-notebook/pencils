Open notebook 1: Pencils on surfaces
====================================

This is an open research notebook.

I plan to post:
- problems I'm thinking about,
- thoughts I've had/progress I've made on these problems.
If you have any thoughts or comments, I'd be very happy to hear from
you (either via email, via bug reports or via a git pull request).

The remit of this particular notebook is the topic of pencils on
algebraic surfaces. My interest in this topic has grown out of
conversations with Giancarlo Urzua and Ivan Smith.

Contents of repository
----------------------

- the documents folder contains a file progress-to-date.pdf, which is
  an unpolished account of what I've been doing and a file
  pencils.pdf, which is an attempt to make this more readable.
- the python folder contains modules for handling covering spaces and
  Lefschetz pencils in Python (building on curver). This now contains
  the data for the bicanonical pencils on various things including the
  Barlow surface.
- the m2 folder contains some Macaulay2 scripts used in studying the
  variation of critical values as we deform some pencils.
- img, outputs, plots, xojfiles are all full of auxiliary content.
- old_code is where I've been keeping old code safe while I modified
  it. This can be safely ignored and will eventually be deleted.

Project status
--------------

I currently have the relations in the mapping class group
corresponding to:
- a certain quadric pencil on a Fermat surface of arbitrary degree.
- the bicanonical pencil on a Z/5-Godeaux surface
- the bicanonical pencil on the double cover of the canonical model of
  the Barlow surface (branched over the nodes).
- the bicanonical pencil on the Barlow surface.
These can be accessed and played with in a Python runtime environment
by running something like:

> from pencils import Pencil
> barlow=Pencil.barlow()
> barlow.check_relation()
True
> len(barlow.vanishing_cycles)
27
> all([vc.is_nonseparating() for vc in barlow.vanishing_cycles])
True

The next steps in the project are:
- Find the global monodromy of the bicanonical pencil on the
  Craighero-Gattazzo surface.
- Compare it with the one for Barlow (are they Hurwitz/conjugation
  equivalent?).
- Lee-Park Godeaux surfaces contain certain rational homology balls by
  construction; these rational homology balls have planar Lefschetz
  fibrations. Can we see these planar Lefschetz fibrations inside the
  bicanonical pencil on the Barlow surface?

Overview of topic
-----------------

A **Godeaux surface** is a (smooth, complex, projective) minimal
[surface of general
type](https://en.wikipedia.org/wiki/Surface_of_general_type) with
$`K^2=1`$ and $`p_g=0`$. If $`X`$ is a Godeaux surface then [Reid]
showed that $`H_1(X;\mathbf{Z})`$ is the torsion group
$`\mathbf{Z}/n`$, where $`n`$ can be $`1,2,3,4,5`$. Examples of
Godeaux surfaces realising each of these values for the torsion is
known, so the moduli space of Godeaux surfaces has at least five
components.

The Godeaux surfaces with $`H_1=0`$ (i.e. $`n=1`$) are historically
interesting for low-dimensional topologists: they provided the first
examples of 4-manifolds which are homeomorphic to a blow-up of the
complex projective plane (at 8 points) but not diffeomorphic to
it. The first of these was discovered by [Barlow], and further
examples were constructed by [Craighero-Gattazzo] and [Lee-Park].

Let us fix $`n`$. [Reid] showed that the moduli space of Godeaux
surfaces with $`H_1=\mathbf{Z}/n`$ is connected (indeed irreducible)
if $`n=3,4,5`$ and conjectured that it is still irreducible when
$`n=1,2`$. It is an open question whether the known examples of
Godeaux surfaces with $`n=1`$ are deformation equivalent.

The main tool we have to study surfaces of general type is by looking
at [pluricanonical linear
systems](https://en.wikipedia.org/wiki/Canonical_ring), that is
rational maps to projective spaces defined by sections of the
[canonical bundle](https://en.wikipedia.org/wiki/Canonical_bundle)
$`K`$ and its powers. Recall that the dimension of the space of
holomorphic sections of $`K^{\otimes m}`$ is called the **mth
plurigenus** $`P_m`$ and $`P_1`$ is also denoted $`p_g`$. By
definition, Godeaux surfaces have $`p_g=0`$, so $`K`$ has no nonzero
holomorphic sections, and the first interesting pluricanonical system
is the bicanonical system ($`m=2`$).

By
[Hirzebruch-Riemann-Roch](https://en.wikipedia.org/wiki/Riemann%E2%80%93Roch_theorem_for_surfaces)
for the trivial line bundle, we have

```math
1-q(X)+p_g=T(X)
```

where $`T(X)`$ is the Todd genus
($`\frac{1}{12}\int_X(c_1^2+c_2)`$ and $`q(X)`$ is the irregularity
(rank of $`H_1`$). Since $`q(X)=0`$ and $`p_g=0`$ for Godeaux
surfaces, the Todd genus of a Godeaux surface is 1. By
Hirzebruch-Riemann-Roch for $`K^{\otimes m}`$, we have

```math
\chi(K^{\otimes m})=\frac{1}{2}m(m-1)K^2+T(X)={m \choose 2}+1,
```

since $`K^2=1`$. [Kawamata-Viehweg
vanishing](https://en.wikipedia.org/wiki/Kawamata%E2%80%93Viehweg_vanishing_theorem)
tells us that the higher sheaf cohomology groups of $`K^{\otimes m}`$
vanish (because $`K`$ is big and nef). Therefore we deduce that the
Euler characteristic $`\chi(K^{\otimes})`$ equals the mth plurigenus,
so

```math
P_m={m \choose 2}+1.
```

In particular, the bicanonical system is a rational map to
$`\mathbf{P}^1`$, in other words a pencil.

If the pencil is Lefschetz (i.e. fibres have at worst nodal
singularities) then it can be encoded combinatorially in terms of its
monodromy factorisation (see, for example, [Auroux] or Section 2 of
[Auroux-Smith] for an exposition). The combinatorial data is an
ordered collection of simple closed loops on the fibre of the pencil
(which is an algebraic curve with marked points at the basepoints of
the pencil). The product of the Dehn twists in these loops is isotopic
to a composite of boundary-parallel twists.

The construction of the monodromy factorisation relies on various
choices:
- an identification of the base with $`\mathbf{P}^1`$ with the
  critical points sitting in specific positions,
- a choice of basepoint in the base,
- an identification of the fibre with a "standard" surface,
- a choice of "vanishing paths" in the base of the pencil.
These choices lead to monodromy factorisations which are related by
**Hurwitz equivalence** (see [Auroux] for details). It seems to me
that if two surfaces are connected by a path in (an irreducible
component of) the moduli space of surfaces and have Lefschetz
bicanonical pencils then their monodromy factorisations should also be
related by Hurwitz equivalence.

**Problem 1.** Verify this guess.

**Question 2.** Compute monodromy factorisations for the bicanonical
pencils of the known Godeaux surfaces. Are they Hurwitz equivalent?

If the answer to Question 2 is "yes" then we deduce that the known
Godeaux surfaces are at least diffeomorphic (in fact, symplectically
deformation equivalent when equipped with their canonical symplectic
forms). This is not currently known.

If the answer to Question 2 is "no" and problem 1 is solved
affirmatively then we deduce that (contrary to Reid's conjecture) the
moduli space of simply-connected Godeaux surfaces is not
irreducible. Note that proving two factorisations are not Hurwitz
equivalent seems like a nontrivial problem.

Either way, the answer is interesting. Moreover, having an explicit
monodromy factorisation would be useful for studying the 4-dimensional
topology of these manifolds.

This open notebook is a summary of my attempts at and progress in
finding monodromy factorisations for these bicanonical pencils (and
notes on what other people have done in this direction).


References
----------

- Auroux, D. The canonical pencils on Horikawa surfaces, Geometry &
  Topology 10 (2006) 2173–2217
  + DOI: 10.2140/gt.2006.10.2173
  + arXiv: https://arxiv.org/abs/math/0605692
  + Auroux computes the monodromy factorisations for two Horikawa
    surfaces which are known to live in different components of the
    moduli space of Horikawa surfaces but are not known to be
    symplectomorphic. The monodromy factorisations are not obviously
    the same, but differ by a "partial twist" corresponding to the
    fact that the surfaces are related by a Luttinger
    surgery. Unfortunately, this is not enough to answer the question
    of whether the surfaces are symplectomorphic.
- [Auroux-Smith] Auroux, D. and Smith, I. Lefschetz pencils, branched
  covers and symplectic invariants, In: Catanese F., Tian G. (eds)
  Symplectic 4-Manifolds and Algebraic Surfaces. Lecture Notes in
  Mathematics, vol 1938. Springer, Berlin, Heidelberg
  + DOI https://doi.org/10.1007/978-3-540-78279-7_1 
  + arXiv: https://arxiv.org/abs/math/0401021
  + A survey of Lefschetz pencil techniques in symplectic
    topology. Section 2 is a basic introduction to Lefschetz pencils.
- [Barlow] Barlow, R. A simply connected surface of general type with
  $`p_g=0`$, Invent. Math. 79 (1985), 293–301.
  + DOI: https://doi.org/10.1007/BF01388974
  + The original paper in which Barlow constructs the Barlow surface.
- [Craighero-Gattazzo] Craighero, P. and Gattazzo, R. Quintic surfaces
  of $`P^3`$ having a non singular model with $`q=p_g=0`$, $`P_2\neq
  0`$, Rendiconti del Seminario Matematico della Universita di Padova,
  tome 91 (1994), p. 185-198
  + NUMDAM: http://www.numdam.org/item/?id=RSMUP_1994__91__185_0
  + The Craighero-Gattazzo surface is constructed. They start by
    finding an explicit quintic surface with four simple elliptic
    singularities (modelled on $`x^2+y^3+z^6=0`$); when you resolve
    these singularities, you get a smooth Godeaux surface.
- [Lee-Park] Lee, Y. and Park, J. A simply connected surface of
  general type with $`p_g=0`$ and $`K^2=2`$, Invent. Math. 2007,
  Volume 170, Issue 3, pp 483–505
  + DOI: https://doi.org/10.1007/s00222-007-0069-7
  + arXiv: https://arxiv.org/abs/math/0609072
  + The classic paper in which surfaces are constructed by
    Q-Gorenstein smoothing of singular surfaces with Wahl
    singularities (i.e. rational blowdown of chains of spheres). The
    construction of a Godeaux surface is in the appendix (Section 7 of
    the arXiv version).
- [Reid] Reid, M. Surfaces with $`p_g=0`$, $`K^2=1`$,
  J. Fac. Sc. Univ. of Tokyo IA 25 (1978), 75–92.
  + [Also available on Reid's
    website](https://homepages.warwick.ac.uk/~masda/Reprint/pg0K21.pdf).
  + General results on Godeaux surfaces, including a complete
    description of those with torsion of order 3, 4, 5.


Further reading
---------------

- Catanese, F. and Pignatelli, R. On simply connected Godeaux
  surfaces, Complex analysis and algebraic geometry, 117–153, de
  Gruyter, Berlin, 2000
  + [Available on Pignatelli's
    website](http://www.science.unitn.it/~pignatel/papers/godeauxdg.pdf)
  + General results about bicanonical pencils on Godeaux surfaces,
    with specific mention of the Barlow and Craighero-Gattazzo cases
    (but the monodromy factorisation is not worked out).
- Dolgachev, I. and Werner, C. A simply connected numerical Godeaux
  surface with ample canonical class, J. Alg. Geom. 8 (1999), 737–764
  + arXiv: https://arxiv.org/abs/alg-geom/9704022
  + An exploration of the Craighero-Gattazzo surface and some pencils
    on it. Note that there is an error in the proof that the
    fundamental group is trivial.
- Lee, Y. Bicanonical pencil of a determinantal Barlow surface,
  Transactions of the American Mathematical Society Vol. 353, No. 3
  (Mar., 2001), pp. 893-905
  + DOI: https://doi.org/10.1090/S0002-9947-00-02609-X
  + Detailed analysis of the bicanonical pencil on Barlow's surface
    (though doesn't explicitly mention the monodromy factorisation).
- Reid, M. A simply connected surface of general type with $`p_g =
  0`$, $`K^2 = 1`$, due to Rebecca Barlow
  + [available on Reid's
    website](https://homepages.warwick.ac.uk/~masda/surf/more/Barlow.pdf).
  + A very concise and explicit construction of Barlow's surface.
