#!/usr/bin/python3

# reg_i should all be the same
# collis_0,1,2,3 are points which collide at
# [0,0,1,0], [1,0,0,0], [0,1,0,0], [0,0,0,1] respectively
reg=[[0, 1, 4, 7, 9, 11, 18, 19, 20, 22, 23, 24, 28, 30, 32, 33, 35,
      36, 38, 44, 45, 46, 47, 49, 55, 56, 57, 58, 59, 61, 62, 64, 65, 69,
      74, 81, 83, 84, 86, 90, 92, 94, 95, 96, 97, 98, 99, 102, 107, 109,
      111, 113, 114, 116, 118, 119, 120],
     [0, 1, 4, 6, 7, 8, 9, 11, 12, 18, 20, 23, 28, 30, 35, 37, 38,
      41, 42, 44, 46, 49, 55, 56, 57, 58, 59, 60, 62, 64, 65, 71, 72, 74,
      81, 85, 86, 91, 92, 94, 95, 96, 97, 98, 99, 102, 105, 107, 108, 109,
      111, 116, 117, 119, 120, 122, 123],
     [0, 2, 3, 4, 7, 8, 9, 13, 14, 19, 21, 22, 23, 27, 29, 33, 36,
      37, 38, 39, 40, 44, 46, 47, 49, 54, 55, 56, 59, 60, 62, 63, 70, 72,
      77, 78, 80, 82, 83, 84, 86, 89, 91, 92, 93, 94, 98, 100, 101, 103,
      105, 107, 114, 115, 117, 122, 123],
     [0, 2, 3, 4, 5, 6, 7, 12, 15, 16, 20, 23, 24, 26, 27, 29, 32,
      33, 36, 39, 42, 47, 49, 50, 54, 59, 61, 62, 63, 64, 65, 68, 70, 71,
      72, 73, 77, 82, 84, 90, 92, 93, 95, 98, 99, 101, 103, 105, 107, 109,
      111, 112, 115, 117, 118, 119, 123]]

collis=[{3, 39, 42, 50, 53, 71, 72, 78, 80, 85, 87, 88, 89, 103, 106, 110, 124},
        {13, 22, 27, 31, 36, 39, 45, 53, 70, 79, 87, 88, 100, 104, 106, 110, 118},
        {5, 17, 18, 30, 31, 41, 52, 57, 68, 81, 85, 87, 95, 104, 108, 110, 118},
        {1, 13, 17, 18, 30, 48, 51, 57, 60, 78, 81, 85, 87, 88, 91, 97, 110}]
for i in range(0,4):
     for j in range(i+1,4):
         print(i,j,collis[i].intersection(collis[j]))
print(collis[0].intersection(collis[1]).intersection(collis[2]).intersection(collis[3]))
