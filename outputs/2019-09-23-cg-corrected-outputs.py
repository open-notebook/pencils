#!/usr/bin/python3

colliders=[{3, 7, 28, 29, 33, 37, 46, 54, 72, 90, 93, 99, 107, 113, 116, 123, 124},
           {5, 9, 10, 19, 42, 48, 49, 57, 60, 70, 75, 76, 83, 86, 101, 102, 105},
           {18, 25, 36, 41, 45, 50, 56, 59, 62, 67, 68, 77, 89, 94, 95, 103, 117},
           {1, 2, 12, 14, 15, 21, 32, 34, 35, 53, 66, 79, 88, 96, 111, 114, 119}]

allpts={i for i in range(0,125)}
for collider in colliders:
    for y in collider:
        allpts.discard(y)
print(allpts)
print(len(allpts))
