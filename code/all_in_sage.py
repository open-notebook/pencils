# Ground field for CG surface
F = var('F')
minp = F^3+F^2-1
NF.<r> = NumberField(minp,
                     embedding = QQbar(minp.roots()[0][0])) # pick a complex root

'''
NF_extender.<G> = PolynomialRing(NF)
factor_1 = (G^4 - (5*r^2+14)*G^3
            + (38*r^2+60*r+79)*G^2
            - (5*r^2+14)*G+1)
factor_2 = (59*G^8 + (51065*r^2-7308*r-24070)*G^7
            + (1904132*r^2+2715049*r-3134629)*G^6
            + (-9934542*r^2-11947505*r+14684907)*G^5
            + (16297001*r^2+18575719*r-23300196)*G^4
            + (-9934542*r^2-11947505*r+14684907)*G^3
            + (1904132*r^2+2715049*r-3134629)*G^2
            + (51065*r^2-7308*r-24070)*G + 59)
print([QQbar(rt) for (rt, mul) in factor_1.roots()])
print([QQbar(rt) for (rt, mul) in factor_2.roots()])
'''

choice = 0
# 0 gives 0
# 1 gives 1
# 2,3,4,5 give roots of factor_1
# 6,7,8,9,10,11,12,13 give roots of factor_2
# 14 will give \infty
if choice in [0,1]:
    ground_field = NF
    QQ_bar_emb = NF.embedding
    r_CG = r
    crit_val = choice
else:
    # Pick critical points of bicanonical pencil
    NF_extender.<G> = PolynomialRing(NF)
    factor_1 = (G^4 - (5*r^2+14)*G^3
                + (38*r^2+60*r+79)*G^2
                - (5*r^2+14)*G+1)
    factor_2 = (59*G^8 + (51065*r^2-7308*r-24070)*G^7
            + (1904132*r^2+2715049*r-3134629)*G^6
            + (-9934542*r^2-11947505*r+14684907)*G^5
            + (16297001*r^2+18575719*r-23300196)*G^4
            + (-9934542*r^2-11947505*r+14684907)*G^3
            + (1904132*r^2+2715049*r-3134629)*G^2
            + (51065*r^2-7308*r-24070)*G + 59)
    if choice in [2,3,4,5]:
        mipol = factor_1
    else:
        mipol = factor_2
        
    NF_extended.<g_aux> = NumberField(mipol)
    ground_field = NF_extended.absolute_field('g')
    from_ext, to_ext = ground_field.structure()
    valid_embs = [ebd
                  for ebd in ground_field.embeddings(QQbar)
                  if ebd(to_ext(r)) == QQbar(r)]
    QQ_bar_emb = valid_embs[choice-2]
    r_CG = to_ext(r)
    crit_val = to_ext(G)
    print(QQ_bar_emb(r_CG),QQ_bar_emb(crit_val))

    
#critical_values =  [rt[0] for rt in factor_1.roots(ring=QQbar)]
#critical_values += [rt[0] for rt in factor_2.roots(ring=QQbar)]
#critical_values += [0,1] # only missing infinity

# Coefficients from CG paper
a = r_CG^2
b = (7*r_CG^2+4*r_CG-6)/(3*r_CG-2)
c = -(225*r_CG^2-156*r_CG-10)/(5*r_CG^2+212*r_CG-163)
e = -(3*r_CG^2+6*r_CG-8)/(3*r_CG^2+r_CG-2)
f = -(34*r_CG^2-18*r_CG-7)/(29*r_CG^2+22*r_CG-33)
m = -(r_CG^2-r_CG+1)/(2*r_CG^2-4*r_CG+1)

# Equation for CG surface

R.<x,y,z,t,u,v,w,s> = PolynomialRing(ground_field, 8)

q_3 = (x+m*y+a*z)^2;
q_2 = (a^2*x^3 + x*y*(b*x+c*y) + m^2*y^3
       + (e*x^2+f*x*y+c*y^2)*z + (b*x+e*y)*z^2+z^3)
q_1 = (2*a*x^3*y + e*x^2*y^2 + 2*a*m*x*y^3
       + (2*a*m*x^3+f*x^2*y+f*x*y^2+2*m*y^3)*z
       + (c*x^2+f*x*y+b*y^2)*z^2 + 2*(m*x+a*y)*z^3)
q_0 = (x^3*y^2 + a^2*x^2*y^3
       + x*y*(2*m*x^2+b*x*y+2*a*y^2)*z
       + (m^2*x^3+c*x^2*y+e*x*y^2+y^3)*z^2
       + (m*x+a*y)^2*z^3)
Q   = q_0 + q_1*t + q_2*t^2 + q_3*t^3

# Tricanonical map

tri_can_1 = u - (a*(x+m*y+a*z)*t^2 + t*x*y
                 + a^3*y*z*t + (6*r_CG^2+3*r_CG-5)*x*z*t/7)
tri_can_2 = v - (a*(y+m*z+a*t)*x^2 + x*y*z
                    + a^3*z*t*x + (6*r_CG^2+3*r_CG-5)*y*t*x/7)
tri_can_3 = w - (a*(z+m*t+a*x)*y^2 + y*z*t
                 + a^3*t*x*y + (6*r_CG^2+3*r_CG-5)*z*x*y/7)
tri_can_4 = s - (a*(t+m*x+a*y)*z^2 + z*t*x
                 + a^3*x*y*z + (6*r_CG^2+3*r_CG-5)*t*y*z/7)


def get_curve(alpha, beta):
    curve = R * [Q,
                 alpha*y*t - beta*x*z,
                 tri_can_1, tri_can_2,tri_can_3, tri_can_4]
    image_curve = curve.elimination_ideal([x,y,z,t]).gens()
    quadric = [eq for eq in image_curve if eq.total_degree() == 2]
    cubic   = [eq for eq in image_curve if eq.total_degree() == 3]
    if quadric[0].monomial_coefficient(u^2) != 0:
        quadric[0] = quadric[0]/quadric[0].monomial_coefficient(u^2)
    return quadric[0], cubic[0]

def better_sign(signed_number):
    if signed_number == 0:
        return 1
    else:
        return sign(signed_number)
    
def discr(coeff_3, coeff_2, coeff_1, coeff_0):
    '''Discriminant of cubic ax^3+bx^2+cx+d'''
    return simplify(expand(coeff_2**2*coeff_1**2 - 4*coeff_3*coeff_1**3
                           - 4*coeff_2**3*coeff_0 - 27*coeff_3**2*coeff_0**2
                           + 18*coeff_3*coeff_2*coeff_1*coeff_0))

def get_hessian(quadric, emb):
    M = [pol.gradient()[4:] for pol in quadric.gradient()[4:]]
    return Matrix(QQbar,[[emb(entry.monomial_coefficient(x^0)/2)
                          for entry in rw]
                         for rw in M])


def get_discrim(quadric, cubic):
    H = Matrix(QQbar,get_hessian(quadric, QQ_bar_emb))
    D,P = H.eigenmatrix_right()
    sigma = Matrix(QQbar, [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
    D = sigma.inverse()*D*sigma
    P = P*sigma
    signs = [better_sign(P[0][d]) for d in [0,1,2,3]]
    P_modified = [[signs[d]*P[c][d] for d in [0,1,2,3]]
                  for c in [0,1,2,3]]
    P_matr = Matrix(QQbar,P_modified)
    evals = [D[k][k] for k in range(0,4)]
    #if time % steps != 0:
    #    # fix evals
    #    eval_change = [abs(evals[k]-evals_current[k])
    #                   for k in [0,1,2,3]]
    #else:
    #    eval_change = [0,0,0,0]

    #evals_current = evals
    # affine chart in P^1 x P^1
    PP.<X,Y> = PolynomialRing(QQbar,2)
    # rational parametrisation of u^2+v^2+w^2+s^2 = 0
    standard_param = matrix([[(X*Y+1)/(2*sqrt(evals[0]))],
                             [(X*Y-1)/(2*sqrt(-evals[1]))],
                             [(X-Y)/(2*sqrt(evals[2]))],
                             [(X+Y)/(2*sqrt(-evals[3]))]])
    new_param_eqs = P_matr * standard_param
    qqubic = cubic.change_ring(QQ_bar_emb)
    trican = qqubic.parent()
    rat_param = trican.hom([0,0,0,0,
                            new_param_eqs[0][0],
                            new_param_eqs[1][0],
                            new_param_eqs[2][0],
                            new_param_eqs[3][0]],PP)
    new_cubic = rat_param(qqubic)
    # Extract coefficients from cubic
    coef_dict = {m : CDF(new_cubic.monomial_coefficient(m))
                 for m in new_cubic.monomials()}
    Z = var('Z')

    # Gather terms of cubic by powers of Y
    coef_0 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==0])
    coef_1 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==1])
    coef_2 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==2])
    coef_3 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==3])

    discrim = discr(coef_3,coef_2,coef_1,coef_0)
    return discrim/discrim.coefficient(Z^12)
