from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import pickle

DEFAULT_ERROR = 0.000000000000001

init_time = 0
time = 0     # start time for vanishing path
steps = 100  # subdivision of vanishing path
choice = 1   # choice of critical points
bug_checking = 0

anim_running = True # global variable used to pause animation
eval_log = []       # global variable for tracking eigenvalues
P_log = []
error_collector = []

def dist(z_1, z_2):
    return abs(z_1 - z_2)

def haus_dist(bs, cs, bt, ct):
    assert len(bs) == len(cs)
    zs = [bs[k] + cs[k]*I for k in range(0,len(bs))]
    assert len(bt) == len(ct)
    zt = [bt[k] + ct[k]*I for k in range(0,len(bs))]
    A = max([min([dist(z_s, z_t) for z_s in zs]) for z_t in zt])
    B = max([min([dist(z_s, z_t) for z_t in zt]) for z_s in zs])    
    return max(A,B)

def rational_approx(beta, number_field_K, err=DEFAULT_ERROR):
    '''Returns a K-rational approximation of beta

    Here:
    - beta is an element of QQbar,
    - number_field_K is a number field
      *with a specified nonreal embedding into QQbar*.
    - err is the maximum error of the rational approximations 
      of the real and nonreal parts of beta.
    '''
    
    alpha = number_field_K._gens[0]
    re_beta, im_beta = beta.real(), beta.imag()
    re_alpha, im_alpha = QQbar(alpha).real(), QQbar(alpha).imag()
    x_coord = n((im_alpha * re_beta
                 - im_beta * re_alpha) / im_alpha).nearby_rational(err)
    y_coord = n(im_beta / im_alpha).nearby_rational(err)
    return x_coord + alpha * y_coord

# Ground field for CG surface
F = var('F')
minp = F^3+F^2-1
rs = [QQbar(rt_data[0]) for rt_data in minp.roots()]
NF.<r> = NumberField(minp, embedding = QQbar(rs[0])) # pick a complex root
basepoint = -2*r

def end_point(choice=0):
    '''Returns a QQ(r)-rational approximation to the chosen critical value:

    choice = 0 gives 0
             1 gives 1
             2,3,4,5 give roots of factor_1
             6,7,8,9,10,11,12,13 give roots of factor_2
             14 gives \infty
    '''
    
    if choice in [0,1]:
        return choice + 0 * r
    elif choice == 14:
        print("Currently we're just approximating" +
              "infinity by a big number (1000000)")
        return 1000000 + 0 * r
    else:
        # Pick critical points of bicanonical pencil
        NF_extender.<G> = PolynomialRing(NF)
        factor_1 = (G^4 - (5*r^2+14)*G^3
                    + (38*r^2+60*r+79)*G^2
                    - (5*r^2+14)*G+1)
        factor_2 = (59*G^8 + (51065*r^2-7308*r-24070)*G^7
                    + (1904132*r^2+2715049*r-3134629)*G^6
                    + (-9934542*r^2-11947505*r+14684907)*G^5
                    + (16297001*r^2+18575719*r-23300196)*G^4
                    + (-9934542*r^2-11947505*r+14684907)*G^3
                    + (1904132*r^2+2715049*r-3134629)*G^2
                    + (51065*r^2-7308*r-24070)*G + 59)
        if choice in [2,3,4,5]:
            mipol = factor_1
        else:
            mipol = factor_2
            
        NF_extended.<g_aux> = NumberField(mipol)
        ground_field = NF_extended.absolute_field('g')
        from_ext, to_ext = ground_field.structure()
        valid_embs = [ebd
                      for ebd in ground_field.embeddings(QQbar)
                      if abs(CDF(ebd(to_ext(r))) - CDF(rs[0])) < 0.1]
        exact_crit_vals = [ebd(to_ext(G))
                           for ebd in valid_embs]
        crit_vals = [rational_approx(cv, NF)
                     for cv in exact_crit_vals]
        if choice in [2,3,4,5]:
            return crit_vals[choice - 2]
        elif choice in [6,7,8,9,10,11,12,13]:
            return crit_vals[choice - 6]



# Coefficients from CG paper
a = r^2
b = (7*r^2+4*r-6)/(3*r-2)
c = -(225*r^2-156*r-10)/(5*r^2+212*r-163)
e = -(3*r^2+6*r-8)/(3*r^2+r-2)
f = -(34*r^2-18*r-7)/(29*r^2+22*r-33)
m = -(r^2-r+1)/(2*r^2-4*r+1)

# Equation for CG surface

R.<x,y,z,t,u,v,w,s> = PolynomialRing(NF, 8)

q_3 = (x+m*y+a*z)^2;
q_2 = (a^2*x^3 + x*y*(b*x+c*y) + m^2*y^3
       + (e*x^2+f*x*y+c*y^2)*z + (b*x+e*y)*z^2+z^3)
q_1 = (2*a*x^3*y + e*x^2*y^2 + 2*a*m*x*y^3
       + (2*a*m*x^3+f*x^2*y+f*x*y^2+2*m*y^3)*z
       + (c*x^2+f*x*y+b*y^2)*z^2 + 2*(m*x+a*y)*z^3)
q_0 = (x^3*y^2 + a^2*x^2*y^3
       + x*y*(2*m*x^2+b*x*y+2*a*y^2)*z
       + (m^2*x^3+c*x^2*y+e*x*y^2+y^3)*z^2
       + (m*x+a*y)^2*z^3)
Q   = q_0 + q_1*t + q_2*t^2 + q_3*t^3

# Tricanonical map

tri_can_1 = u - (a*(x+m*y+a*z)*t^2 + t*x*y
                 + a^3*y*z*t + (6*r^2+3*r-5)*x*z*t/7)
tri_can_2 = v - (a*(y+m*z+a*t)*x^2 + x*y*z
                 + a^3*z*t*x + (6*r^2+3*r-5)*y*t*x/7)
tri_can_3 = w - (a*(z+m*t+a*x)*y^2 + y*z*t
                 + a^3*t*x*y + (6*r^2+3*r-5)*z*x*y/7)
tri_can_4 = s - (a*(t+m*x+a*y)*z^2 + z*t*x
                 + a^3*x*y*z + (6*r^2+3*r-5)*t*y*z/7)


def get_curve(alpha, beta):
    '''Returns quadric and cubic cutting out canonical image of bicanonical curve 
    living over for [alpha:beta] in P^1(NF)'''
    curve = R * [Q,
                 alpha*y*t - beta*x*z,
                 tri_can_1, tri_can_2,tri_can_3, tri_can_4]
    image_curve = curve.elimination_ideal([x,y,z,t]).gens()
    quadric = [eq for eq in image_curve if eq.total_degree() == 2]
    cubic   = [eq for eq in image_curve if eq.total_degree() == 3]
    if quadric[0].monomial_coefficient(u^2) != 0:
        quadric[0] = quadric[0]/quadric[0].monomial_coefficient(u^2)
    if cubic[0].monomial_coefficient(u*v^2) != 0:
        cubic[0] = cubic[0]/cubic[0].monomial_coefficient(u*v^2)
    return quadric[0], cubic[0]

'''
def discr(coeff_3, coeff_2, coeff_1, coeff_0):
    ''Discriminant of cubic ax^3+bx^2+cx+d''
    return simplify(expand(coeff_2**2*coeff_1**2 - 4*coeff_3*coeff_1**3
                           - 4*coeff_2**3*coeff_0 - 27*coeff_3**2*coeff_0**2
                           + 18*coeff_3*coeff_2*coeff_1*coeff_0))
'''

def get_hessian(quadric):
    M = [pol.gradient()[4:] for pol in quadric.gradient()[4:]]
    return Matrix(CDF,[[QQbar(entry.monomial_coefficient(x^0)/2)
                        for entry in rw]
                       for rw in M])


'''
def better_sign(signed_number):
    if abs(signed_number.real()) < DEFAULT_ERROR:
        return 1
    else:
        return sign(signed_number.real())
'''

def get_rat_param(quadric):
    global eval_log
    global P_log
    global bug_checking
    global time
    H = get_hessian(quadric)
    np_evals, np_evecs = np.linalg.eig(H)
    evals = [evalu for evalu in np_evals]
    evecs = [[np_evecs[l, k]
              for l in range(0,len(np_evecs))]
             for k in range(0,len(np_evecs))]
    predicted_indices = [k for k in range(0,len(evals))]
    if len(eval_log) >= 2:
        # order evals so all are moving in straightish line.
        evals_0 = eval_log[time - 2]
        evals_1 = eval_log[time - 1]
        predicted_evals = [2*evals_1[k] - evals_0[k] for k in range(0,len(evals))]
        for k in range(0,len(evals)):
            discrepancies = [abs(pred - evals[k])
                             for pred in predicted_evals]
            predicted_indices[k] = discrepancies.index(min(discrepancies))
    elif len(eval_log) == 1:
        # order evals to be close to previous
        predicted_evals = eval_log[0]
        for k in range(0,len(evals)):
            discrepancies = [abs(pred - evals[k])
                             for pred in predicted_evals]
            predicted_indices[k] = discrepancies.index(min(discrepancies))
        
    evals = [evals[predicted_indices[k]] for k in range(0,len(evals))]
    evecs = [evecs[predicted_indices[k]] for k in range(0,len(evals))]
    eval_log += [evals]
    if bug_checking == 1:
        print("evals: ", evals)
    D = diagonal_matrix(evals)
    P = Matrix(CDF, evecs).transpose()
    #D,P = H.eigenmatrix_right()
    #sigma = Matrix(CDF, [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]])
    #D = sigma.inverse()*D*sigma
    #P = P*sigma
    if len(P_log) >= 2:
        # P_log is first row of P_matr
        P_0 = P_log[time - 2]
        P_1 = P_log[time - 1]
        predicted_Ps = [2*P_1[k] - P_0[k]
                        for k in range(0,len(P[0]))]
        phases = [exp(I*arg(predicted_Ps[k]/P[0][k]))
                  for k in range(0,len(P[0]))]
        #print(time, predicted_Ps, P, phases)
    elif len(P_log) == 1:
        phases = [exp(I*arg(P_log[0][k]/P[0][k]))
                  for k in range(0,len(P[0]))]
        #print(time, P[0], phases)
    else:
        phases = [1 for d in range(0,len(P[0]))]
        #print(time, P[0])
    P_modified = [[phases[d]*P[c][d]
                   for d in [0,1,2,3]]
                  for c in [0,1,2,3]]
    P_matr = Matrix(CDF, P_modified)
    #print(P_matr[0])
    P_log += [P_matr[0]]
    #evals = [D[k][k] for k in range(0,4)]
    #if time % steps != 0:
    #    # fix evals
    #    eval_change = [abs(evals[k]-evals_current[k])
    #                   for k in [0,1,2,3]]
    #else:
    #    eval_change = [0,0,0,0]

    #evals_current = evals
    # affine chart in P^1 x P^1
    PP.<X,Y> = PolynomialRing(CDF,2)
    # rational parametrisation of u^2+v^2+w^2+s^2 = 0
    standard_param = matrix([[(X*Y+1)/(2*sqrt(evals[0]))],
                             [(X*Y-1)/(2*sqrt(-evals[1]))],
                             [(X-Y)/(2*sqrt(evals[2]))],
                             [(X+Y)/(2*sqrt(-evals[3]))]])
    new_param_eqs = P_matr * standard_param

    def change_coords(polyn):
        polyn_CDF = polyn.change_ring(CDF)
        trican = polyn_CDF.parent()
        rat_param = trican.hom([0,0,0,0,
                                new_param_eqs[0][0],
                                new_param_eqs[1][0],
                                new_param_eqs[2][0],
                                new_param_eqs[3][0]],PP)
        return rat_param(polyn_CDF)

    return change_coords
    
'''
def get_discrim(quadric, cubic):
    changer = get_rat_param(quadric)
    new_cubic = changer(cubic)
    return new_cubic

    # Extract coefficients from cubic
    coef_dict = {m : new_cubic.monomial_coefficient(m)
                 for m in new_cubic.monomials()}
    # The fix below has now been implemented earlier
    # while the cubic is still exact:
    # scale_factor = max([coef_dict[m] for m in
    #coef_dict]) coef_dict = {m: coef_dict[m]/scale_factor for m in
    #coef_dict}
    Z = var('Z')

    # Gather terms of cubic by powers of Y
    coef_0 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==0])
    coef_1 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==1])
    coef_2 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==2])
    coef_3 = sum([coef_dict[m]*Z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==3])
    
    discrim = discr(coef_3,coef_2,coef_1,coef_0)
    return discrim/discrim.coefficient(Z^12)
'''
'''
def get_plot(alpha, beta):
    ''Returns the critical values of a trigonal projection for
    the bicanonical curve specified by pencil_val.''
    discrim = get_discrim(*get_curve(alpha, beta))
    branch_pts = [rt[0] for rt in discrim.roots(ring=CDF)]
    xs = [bp.real() for bp in branch_pts]
    ys = [bp.imag() for bp in branch_pts]
    return xs, ys
'''


GaussInt.<J> = NumberField(F^2+1, embedding = I)
FinalRing.<U,V> = PolynomialRing(GaussInt)
W = var('W')

def QQify(term):
    global U
    global V
    if term in CDF:
        return (Rational(term.real())+J*Rational(term.imag()))
    else:
        new_poly = 0*U
        old_X = term.parent().gens()[0]
        old_Y = term.parent().gens()[1]
        for deg_X in range(0,4):
            for deg_Y in range(0,4):
                
                new_poly += QQify(CDF(term.coefficient({old_X: deg_X,
                                                        old_Y: deg_Y})))*U^(deg_X)*V^(deg_Y)
        return new_poly

def get_branch_points(quadric, cubic):
    changer = get_rat_param(quadric)
    new_cubic = QQify(changer(cubic))
    crit_ideal = FinalRing*[new_cubic,
                            new_cubic.derivative(V)]
    discrimi = crit_ideal.elimination_ideal([V]).gens()[0]
    output_poly = 0*W
    scalar = discrimi.monomial_coefficient(U^12)
    for deg_W in range(0,13):
        output_poly += CDF(discrimi.monomial_coefficient(U^(deg_W))/scalar) * W^(deg_W)
    return [rt[0] for rt in output_poly.roots(ring=CDF)]

def ans(tim, basepoint, endpt, steps):
    return get_branch_points(*get_curve(basepoint +
                                        (endpt - basepoint) * tim / steps, 1))

# Animation

fig, ax = plt.subplots()
ax.set_xlim([-3.5,1.5])
ax.set_ylim([-3,3])
endpt = end_point(choice)

motion = []
time = init_time
for tim in range(init_time,steps):
    zs = ans(tim, basepoint, endpt, steps)
    xs = [zr.real() for zr in zs]
    ys = [zi.imag() for zi in zs]
    #print(time)
    #pencil_pt = basepoint + (endpt - basepoint)*time/steps
    #xs, ys = get_plot(pencil_pt, 1)
    if time > 1:
        # gromov-hausdorff
        xt = motion[-1][0]
        yt = motion[-1][1]
        error_collector += [haus_dist(xs,ys,xt,yt)]
    motion += [[xs,ys]]
    time = (time + 1) % steps
    
colours = cm.rainbow(np.linspace(0, 1, steps+1))
for moment, colour in zip(motion, colours):
    ax.scatter(moment[0], moment[1], color=colour)

plt.show()


#xs, ys = get_plot(basept+(endpt-basept)*time/steps,1)
#scat = ax.scatter(xs, ys)
#xs, ys, _ = get_plot(1)
#scat1 = ax.scatter(xs, ys, c= 'red')
#xs, ys, _ = get_plot(2-99/100)
#scat2 = ax.scatter(xs, ys)
#plt.show()
'''
def on_click(event):
    if event.dblclick:
        global anim_running
        if anim_running:
            anim.event_source.stop()
            anim_running = False
        else:
            anim.event_source.start()
            anim_running = True
            
def animate(tim):
    global time
    global steps
    if time >= steps - 1:
        time = steps
    else:
        time = (time + 1) % steps
    #xs, ys, err = get_plot(2-time/steps)
    pencil_pt = basepoint + (endpt - basepoint) * time / steps
    xs, ys = get_plot(pencil_pt, 1)
    #plt.title(str(float(2-time/steps))+" : "+str(err))

    plt.title(str(time)+ " / " + str(steps))
    scat.set_offsets(np.c_[xs, ys])
    return scat

motion = []
time = init_time
for tim in range(init_time,steps):
    #print(time)
    pencil_pt = basepoint + (endpt - basepoint)*time/steps
    xs, ys = get_plot(pencil_pt, 1)
    if time > 1:
        # gromov-hausdorff
        xt = motion[-1][0]
        yt = motion[-1][1]
        error_collector += [haus_dist(xs,ys,xt,yt)]
    motion += [[xs,ys]]
    time = (time + 1) % steps
    
colours = cm.rainbow(np.linspace(0, 1, steps+1))
for moment, colour in zip(motion, colours):
    ax.scatter(moment[0], moment[1], color=colour)


#fig.canvas.mpl_connect('button_press_event', on_click)
#anim = FuncAnimation(fig, animate, interval = 1, frames=100)
#pickle.dump(fig, open('test.pickle', 'wb'))    
plt.show()
'''
