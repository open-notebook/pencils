#!/usr/bin/python3

"""Module for taking (finite, unbranched) covering spaces of a surface."""

from curver.kernel.triangulation import Triangulation, Triangle, Edge
import curver


class Surface(Triangulation):
    """A class representing 2-d surfaces.

    This builds on curver's Triangulation class to add methods for
    taking covering spaces.

    """

    def __init__(self, triangles):
        """Create a surface."""
        triangles_for_curver = [Triangle([Edge(e) for e in tri])
                                for tri in triangles]
        super().__init__(triangles_for_curver)

    def get_cover(self, sheets, perms):
        """Return a covering space.

        Input:
        - a list of sheets
        - a dictionary of monodromy permutations (associated to edges)

        This permutation dictionary can be partially specified (and we
        assume the remaining permutations are assumed to be the
        identity). They should be functions (i.e. callable),
        e.g. lambda x: x.

        Triangles (resp. edges) upstairs are labelled by pairs (t, x)
        (resp. (e, x)) where t is a triangle (resp. edge) downstairs
        and x is a sheet.

        Let t and t' be the triangles downstairs which contain the edges e and
        ~e respectively. Upstairs, we glue:

        edge (e, x) in triangle (t, x)

        to

        edge (~e, x) in triangle (t', sigma[e]^{-1}(x))

        as shown in this picture:

                          |
                          ^
                  (t, x)  |  (t', sigma[e]^{-1}(x))
                          |
                   (e, x) | ~(e, x)
                          |
                          |

        """
        edges_downstairs = self.indices
        edges_upstairs = [(e, x)
                          for x in sheets
                          for e in edges_downstairs]

        def pick_edge(edge, sheet):
            return edges_upstairs.index((edge, sheet))

        # If monodromy over an edge has not been specified,
        # assume it's the identity
        for e in edges_downstairs:
            if e not in perms.keys():
                perms.update({e: lambda x: x})

        def reattach(edge, sheet):
            if str(edge)[0] == '~':
                e = int(str(edge)[1::])
                return ~pick_edge(e, perms[e](sheet))
            else:
                return pick_edge(edge, sheet)

        preglued_triangles = [[(tri[0], x), (tri[1], x), (tri[2], x)]
                              for tri in self.triangles
                              for x in sheets]
        glued_triangles = [[reattach(*edge)
                            for edge in tri]
                           for tri in preglued_triangles]

        covering_map = {pick_edge(e, x): e
                        for e in edges_downstairs
                        for x in sheets}

        return CoveringSpace(glued_triangles, self, covering_map)


class CoveringSpace(Surface):
    """A class for covering spaces."""

    def __init__(self, triangles, target, covering_map):
        """Create a CoveringSpace.

        Input is a covering map from source to target. Source and
        target should be curver.Triangulations; covering_map should be
        a dictionary sending edges of source to edges of target.

        """
        super().__init__(triangles)
        self.target = target
        self.covering_map = covering_map
        lifts_of_edge_0 = [p
                           for p in self.indices
                           if covering_map[p] == 0]
        self.number_of_sheets = len(lifts_of_edge_0)

    def preimage(self, lam):
        """Take preimage of lamination under covering map."""
        return self.lamination([lam.geometric[self.covering_map[p]]
                                for p in self.indices])

    def deck_group(self):
        """Return the deck group of the covering space."""
        # Pick edge 0 downstairs and look at all its preimages
        lifts_of_edge_0 = [p
                           for p in self.indices
                           if self.covering_map[p] == 0]
        # Pick one of these preimages
        base_edge = lifts_of_edge_0[0]
        deck_transformations = []
        for e in lifts_of_edge_0:
            # For each preimage e of edge 0, look for a deck
            # transformation that sends base_edge to e
            try:
                deck_trans = self.find_isometry(self,
                                                {base_edge: e})
            except ValueError:  # Case: No such deck transformation
                pass
            else:  # Case: Deck transformation found
                # Add it to the deck group if found
                deck_transformations.append(deck_trans)
        return deck_transformations

    def is_galois(self):
        """Test to see if the deck group acts transitively."""
        return self.number_of_sheets == len(self.deck_group())


if __name__ == "__main__":
    # Simple tests/examples
    # The thrice-punctured sphere
    T = Surface([[0, 1, ~0], [2, ~2, ~1]])
    # A double cover
    S = T.get_cover([0, 1], {0: lambda x: (x+1) % 2})
    print(S.surface())  # Four puncture sphere
    print(S.is_galois())  # True
    # Some curves:
    alpha = [T.lamination([1, 0, 0]), T.lamination([0, 0, 1])]
    # Their preimages
    beta = [S.preimage(c) for c in alpha]
    # 1 => Connected, 2 => Disconnected
    print([len(c.components().keys()) for c in beta])
    curver.show(alpha+beta)
