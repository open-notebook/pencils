#
#
# Biggest problem is to sort out choice of rational
# parametrisation by fixing the diagonalisation properly
#
# Also, create animation for vanishing paths
#
#
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
time = 27
steps = 1000
evals_current = [0,0,0,0]

def better_sign(x):
    if x == 0:
        return 1
    else:
        return sign(x)
    
def test_1(x):
    if x<0.54:
        return True
    else:
        return False

def test_2(x):
    if x < 0.87 and x >= 0.54:
        return True
    else:
        return False

def test_3(x):
    if x < 1 and x>=0.87:
        return True
    else:
        return False

def test_0(x):
    if x ==0:
        return True
    else:
        return False

def test_4(x):
    if x>=1:
        return True
    else:
        return False
    
which_perm_dict = {0: test_0,
                   1: test_1,
                   2: test_2,
                   3: test_3,
                   4: test_4}

Q = Matrix(CDF,[[1,0,0,0],
                [0,1,0,0],
                [0,0,1,0],
                [0,0,0,1]])
perm_mats = [Q,Q,Q,Q,Q]

'''perm_mats = [Q*Matrix(CDF,[[1,0,0,0],
                           [0,1,0,0],
                           [0,0,1,0],
                           [0,0,0,1]]),
             Q*Matrix(CDF,[[1,0,0,0],
                           [0,0,1,0],
                           [0,1,0,0],
                           [0,0,0,1]]),
             Q*Matrix(CDF,[[1,0,0,0],
                           [0,0,1,0],
                           [0,0,0,1],
                           [0,1,0,0]]),
             Q*Matrix(CDF,[[0,0,0,1],
                           [0,1,0,0],
                           [0,0,1,0],
                           [1,0,0,0]]),
             Q*Matrix(CDF,[[0,0,1,0],
                           [0,0,0,1],
                           [0,1,0,0],
                           [1,0,0,0]])]
'''          

# Set up the ring of sections of the tricanonical bundle
singular.eval('ring trican = (complex,50),(u,v,w,s),dp;')
_ = singular.LIB("/home/jde27/maths/pencils/code/cg_bican.lib")
# Let's try and do the vanishing path from 1 to 0

def get_plot(pencil_val):
    global time
    global steps
    global evals_current
    global which_perm_dict
    global perm_mats
    # Get the quadric and cubic for our genus 4 curve
    quadric_cubic = singular.getQuadricCubic("1",'"'+str(pencil_val)+'"','"1"',"1")
    quadric = quadric_cubic[1]
    cubic = quadric_cubic[2]
    # Diagonalise the quadric and thereby
    # find a rational parametrisation by P^1 x P^1
    hess = quadric.jacob().jacob()/2
    H = Matrix(CDF,[[c for c in K] for K in hess.sage()])
    D,P = H.eigenmatrix_right()
    which_perm = [c for c in which_perm_dict if which_perm_dict[c](time/steps)]
    sigma = perm_mats[which_perm[0]]
    D = sigma.inverse()*D*sigma
    P = P*sigma
    signs = [better_sign(P[0][d]) for d in [0,1,2,3]]
    P_modified = [[signs[d]*P[c][d] for d in [0,1,2,3]]
                  for c in [0,1,2,3]]
    P_matr = Matrix(CDF,P_modified)
    #thetas = [-arg(P[c][1]) for c in [0,1,2,3]]
    #U_vals = [[0 for c in [0,1,2,3]]
    #          for d in [0,1,2,3]]
    #for c in [0,1,2,3]:
    #    U_vals[c][c] = exp(thetas[c])
    #U = Matrix(CDF,[[U_vals[c][d] for c in [0,1,2,3]]
    #                for d in [0,1,2,3]])
    #P = P*U
    evals = [D[k][k] for k in range(0,4)]
    if time % steps != 0:
        # fix evals
        eval_change = [abs(evals[k]-evals_current[k])
                       for k in [0,1,2,3]]
    else:
        eval_change = [0,0,0,0]

    evals_current = evals
    # affine chart in P^1 x P^1
    PP.<x,y> = PolynomialRing(CDF,2)
    # rational parametrisation of u^2+v^2+w^2+s^2 = 0
    standard_param = matrix([[(x*y+1)/(2*sqrt(evals[0]))],
                             [(x*y-1)/(2*I*sqrt(evals[1]))],
                             [(x-y)/(2*sqrt(evals[2]))],
                             [(x+y)/(2*I*sqrt(evals[3]))]])
    new_param_eqs = P_matr * standard_param
    trican = cubic.sage().parent()
    rat_param = trican.hom([new_param_eqs[0],
                            new_param_eqs[1],
                            new_param_eqs[2],
                            new_param_eqs[3]],PP)
    # Pullback the cubic along the rational parametrisation
    new_cubic = rat_param(cubic.sage())
    # Extract coefficients from cubic
    coef_dict = {m : new_cubic.monomial_coefficient(m)
                 for m in new_cubic.monomials()}
    z = var('z')
    # Gather terms of cubic by powers of y
    coef_0 = sum([coef_dict[m]*z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==0])
    coef_1 = sum([coef_dict[m]*z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==1])
    coef_2 = sum([coef_dict[m]*z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==2])
    coef_3 = sum([coef_dict[m]*z**(m.exponents()[0][0])
                  for m in coef_dict
                  if m.exponents()[0][1]==3])
    
    def discr(a,b,c,d):
        '''Discriminant of cubic ax^3+bx^2+cx+d'''
        return b**2*c**2-4*a*c**3-4*b**3*d-27*a**2*d**2+18*a*b*c*d
    
    discrim = discr(coef_3,coef_2,coef_1,coef_0)
    branch_pts = [rt[0] for rt in discrim.roots(ring=CDF)]
    xs = [bp.real() for bp in branch_pts]
    ys = [bp.imag() for bp in branch_pts]
    '''print(time)
    print(evals_current)
    print(P_matr)'''
    return xs, ys, max(eval_change)

#fig, ax =plt.subplots()
#colours = cm.rainbow(np.linspace(0, 1, 51))
#for t in range(1,50):
#    ax.scatter(*get_plot(t/50),c=colours[t])
#
#plt.show()

anim_running = True # global variable used to pause animation
fig, ax = plt.subplots()
ax.set_xlim([-3.5,1.5])
ax.set_ylim([-3,3])
xs, ys, _ = get_plot(2)
scat = ax.scatter(xs, ys)
#xs, ys, _ = get_plot(1)
#scat1 = ax.scatter(xs, ys, c= 'red')
#xs, ys, _ = get_plot(2-99/100)
#scat2 = ax.scatter(xs, ys)
#plt.show()

def on_click(event):
    if event.dblclick:
        global anim_running
        if anim_running:
            anim.event_source.stop()
            anim_running = False
        else:
            anim.event_source.start()
            anim_running = True
            
def animate(tim):
    global time
    global steps
    if time >= steps - 1:
        time = steps
    else:
        time = (time + 1) % steps
    #xs, ys, err = get_plot(2-time/steps)
    xs, ys, err = get_plot(2+10*time/steps)
    #plt.title(str(float(2-time/steps))+" : "+str(err))
    plt.title(str(float(2+10*time/steps))+" : "+str(err))
    scat.set_offsets(np.c_[xs, ys])
    return scat

fig.canvas.mpl_connect('button_press_event', on_click)
anim = FuncAnimation(fig, animate, interval = 1, frames=100)
    
plt.show()
