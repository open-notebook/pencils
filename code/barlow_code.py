R.<x_0,x_1,x_2,x_3,x_4,y_0,y_1,y_2,y_3,y_4,W,X,Y,Z> = PolynomialRing(QQ, 14)

xs = [x_0, x_1, x_2, x_3, x_4]
ys = [y_0, y_1, y_2, y_3, y_4]

A1 = matrix([[-x_4, x_2-x_0-x_4, x_0-x_1-x_4, x_3-x_2-x_4, x_1-x_3-x_4],
             [0,    -x_0,        x_3-x_1-x_0, x_1-x_2-x_0, x_4-x_3-x_0],
             [0,    0,           -x_1,        x_4-x_2-x_1, x_2-x_3-x_1],
             [0,    0,           0,           -x_2,        x_0-x_3-x_2],
             [0,    0,           0,           0,           -x_3]])
A = A1 + A1.transpose()
B = A.adjoint()

y_eqs = [sum([A[i,j]*ys[j]
              for j in range(0,5)])
         for i in range(0,5)]
yy_eqs = [ys[i]*ys[j] - B[i,j]
          for i in range(0,5) for j in range(0,5)]

F = R * ([sum(xs)] + y_eqs + yy_eqs)

xi = [x_1*x_2 + x_2*x_3 + x_3*x_4 + x_4*x_0 + x_0*x_1,
      x_1*x_3 + x_2*x_4 + x_3*x_0 + x_4*x_1 + x_0*x_2,
      x_1^2 + x_2^2 + x_3^2 + x_4^2 + x_0^2]
eta = [x_1*x_2*x_3 + x_2*x_3*x_4 + x_3*x_4*x_0 + x_4*x_0*x_1 + x_0*x_1*x_2,
       x_1*x_2*x_4 + x_2*x_3*x_0 + x_3*x_4*x_1 + x_4*x_0*x_2 + x_0*x_1*x_3,
       x_1^2*(x_2+x_0) + x_2^2*(x_3+x_1) + x_3^2*(x_4+x_2) + x_4^2*(x_0+x_3) + x_0^2*(x_1+x_4),
       x_1^3 + x_2^3 + x_3^3 + x_4^3 + x_0^3,
       x_1^2*(x_3+x_4) + x_2^2*(x_4+x_0) + x_3^2*(x_0+x_1) + x_4^2*(x_1+x_2) + x_0^2*(x_2+x_3)]
zeta = [sum([xs[i]*ys[(i+j)%5]
             for i in range(0,5)])
        for j in range(0,5)]

list_of_eqs = [2*xi[0] + 2*xi[1] + xi[2],
               2*eta[0] + eta[1] + eta[2],
               eta[0]+ 2*eta[1] + eta[4],
               eta[3] + eta[2] + eta[4],
               zeta[0] + zeta[1] + zeta[2] + zeta[3] + zeta[4],
               zeta[1]]
act_1 = R.hom([x_1,x_2,x_3,x_4,x_0,
               y_1,y_2,y_3,y_4,y_0,
               W,X,Y,Z], R)
act_2 = R.hom([x_3,x_2,x_1,x_0,x_4,
               -y_0,-y_4,-y_3,-y_2,-y_1,
               W,X,Y,Z], R)
print(not(any([F.reduce(eq) for eq in list_of_eqs])))
print(act_1(F) == F)
print(act_2(F) == F)

curve = R * ([xi[0] - xi[1],
              W - eta[0],
              X - eta[1],
              Y - zeta[0] + zeta[2],
              Z - zeta[3] + zeta[4],
              sum(xs)] + y_eqs + yy_eqs)
canonical_model = curve.elimination_ideal(xs+ys).gens()
