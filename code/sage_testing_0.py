#!/usr/bin/python

from sage.all import *

F = var('F')
minp = F^3+F^2-1
rs = [QQbar(rt_data[0]) for rt_data in minp.roots()]
#NF.<r> = NumberField(minp, embedding = QQbar(rs[0])) # pick a complex root

print(rs)
