QR<x> := PolynomialRing(Rationals(),1);
K<r> := NumberField(x^3+x^2-1);
AA<X,Y,Z,T,U,V,W,S,A,B> := PolynomialRing(K,10);
a := r^2;
b := (7*r^2+4*r-6)/(3*r-2);
c := -(225*r^2-156*r-10)/(5*r^2+212*r-163);
e := -(3*r^2+6*r-8)/(3*r^2+r-2);
f := -(34*r^2-18*r-7)/(29*r^2+22*r-33);
m := -(r^2-r+1)/(2*r^2-4*r+1);
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
eq_0 := U - (a*(X+m*Y+a*Z)*T^2 + T*X*Y + a^3*Y*Z*T + (6*r^2+3*r-5)*X*Z*T/7);
eq_1 := V - (a*(Y+m*Z+a*T)*X^2 + X*Y*Z + a^3*Z*T*X + (6*r^2+3*r-5)*Y*T*X/7);
eq_2 := W - (a*(Z+m*T+a*X)*Y^2 + Y*Z*T + a^3*T*X*Y + (6*r^2+3*r-5)*Z*X*Y/7);
eq_3 := S - (a*(T+m*X+a*Y)*Z^2 + Z*T*X + a^3*X*Y*Z + (6*r^2+3*r-5)*T*Y*Z/7);
eq_4 := A*Y*T-B*X*Z;
I := ideal<AA | Q,eq_0,eq_1,eq_2,eq_3,eq_4>;
J := EliminationIdeal(I, 4);



QR<x> := PolynomialRing(Rationals());
K<r> := NumberField(x^3+x^2-1);
AA<X,Y,Z,T,U,V,W,S> := PolynomialRing(K,10);
a := r^2;
b := (7*r^2+4*r-6)/(3*r-2);
c := -(225*r^2-156*r-10)/(5*r^2+212*r-163);
e := -(3*r^2+6*r-8)/(3*r^2+r-2);
f := -(34*r^2-18*r-7)/(29*r^2+22*r-33);
m := -(r^2-r+1)/(2*r^2-4*r+1);
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
eq_0 := U - (a*(X+m*Y+a*Z)*T^2 + T*X*Y + a^3*Y*Z*T + (6*r^2+3*r-5)*X*Z*T/7);
eq_1 := V - (a*(Y+m*Z+a*T)*X^2 + X*Y*Z + a^3*Z*T*X + (6*r^2+3*r-5)*Y*T*X/7);
eq_2 := W - (a*(Z+m*T+a*X)*Y^2 + Y*Z*T + a^3*T*X*Y + (6*r^2+3*r-5)*Z*X*Y/7);
eq_3 := S - (a*(T+m*X+a*Y)*Z^2 + Z*T*X + a^3*X*Y*Z + (6*r^2+3*r-5)*T*Y*Z/7);
eq_4 := Y*T-2*X*Z;
Quadric := ideal<AA | eq_0,eq_1,eq_2,eq_3,eq_4>;
CG := ideal<AA | eq_0,eq_1,eq_2,eq_3,Q>;
QuadricImage := EliminationIdeal(Quadric, 4);
CGImage := EliminationIdeal(CG,4);
Inters := QuadricImage+CGImage;




// Just the CG surface
P<X,Y,Z,T> := ProjectiveSpace(K,3);
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
CG := Scheme(P,Q);
// and its blow up at a point
pt := Scheme(P,[X,Y,Z]);
CGB := Blowup(CG,pt : Ordinary:=false);


//tricanonical model
C_1 := T*((3*r-2)*(X+m*Y+r^2*Z)*T + (r+1)*X*Y + (-6*r^2+2*r+2)*X*Z + (-2*r^2-5*r+5)*Y*Z);
C_2 := X*((3*r-2)*(Y+m*Z+r^2*T)*X + (r+1)*Y*Z + (-6*r^2+2*r+2)*Y*T + (-2*r^2-5*r+5)*Z*T);
C_3 := Y*((3*r-2)*(Z+m*T+r^2*X)*Y + (r+1)*Z*T + (-6*r^2+2*r+2)*Z*X + (-2*r^2-5*r+5)*T*X);
C_4 := Z*((3*r-2)*(T+m*X+r^2*Y)*Z + (r+1)*T*X + (-6*r^2+2*r+2)*T*Y + (-2*r^2-5*r+5)*X*Y);
P_tri<U,V,W,S> := ProjectiveSpace(K,3);
tri_can := map< P -> P_tri | [C_1, C_2, C_3, C_3]>;

//tricanonical graph
PP<X,Y,Z,T,U,V,W,S> := ProductProjectiveSpace(K,[3,3]);
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
C_1 := T*((3*r-2)*(X+m*Y+r^2*Z)*T + (r+1)*X*Y + (-6*r^2+2*r+2)*X*Z + (-2*r^2-5*r+5)*Y*Z);
C_2 := X*((3*r-2)*(Y+m*Z+r^2*T)*X + (r+1)*Y*Z + (-6*r^2+2*r+2)*Y*T + (-2*r^2-5*r+5)*Z*T);
C_3 := Y*((3*r-2)*(Z+m*T+r^2*X)*Y + (r+1)*Z*T + (-6*r^2+2*r+2)*Z*X + (-2*r^2-5*r+5)*T*X);
C_4 := Z*((3*r-2)*(T+m*X+r^2*Y)*Z + (r+1)*T*X + (-6*r^2+2*r+2)*T*Y + (-2*r^2-5*r+5)*X*Y);
TCG := Scheme(PP, [Q, U*C_2-V*C_1,U*C_3-W*C_1,U*C_4-S*C_1, V*C_3-W*C_2, V*C_4-S*C_2, W*C_4-S*C_3]);




//...in affine patch
QR<x> := PolynomialRing(Rationals());
K<r> := NumberField(x^3+x^2-1);
a := r^2;
b := (7*r^2+4*r-6)/(3*r-2);
c := -(225*r^2-156*r-10)/(5*r^2+212*r-163);
e := -(3*r^2+6*r-8)/(3*r^2+r-2);
f := -(34*r^2-18*r-7)/(29*r^2+22*r-33);
m := -(r^2-r+1)/(2*r^2-4*r+1);
PPAff<X,Y,Z,U,V,W> := AffineSpace(K,6);
T:=1;
S:=1;
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
C_1 := T*((3*r-2)*(X+m*Y+r^2*Z)*T + (r+1)*X*Y + (-6*r^2+2*r+2)*X*Z + (-2*r^2-5*r+5)*Y*Z);
C_2 := X*((3*r-2)*(Y+m*Z+r^2*T)*X + (r+1)*Y*Z + (-6*r^2+2*r+2)*Y*T + (-2*r^2-5*r+5)*Z*T);
C_3 := Y*((3*r-2)*(Z+m*T+r^2*X)*Y + (r+1)*Z*T + (-6*r^2+2*r+2)*Z*X + (-2*r^2-5*r+5)*T*X);
C_4 := Z*((3*r-2)*(T+m*X+r^2*Y)*Z + (r+1)*T*X + (-6*r^2+2*r+2)*T*Y + (-2*r^2-5*r+5)*X*Y);
TCGFull := Scheme(PPAff, [Q, U*C_2-V*C_1,U*C_3-W*C_1,U*C_4-S*C_1, V*C_3-W*C_2, V*C_4-S*C_2, W*C_4-S*C_3]);
BadBit := Scheme(PPAff,[X,Y,Z]);
TCG := Complement(TCGFull, BadBit);



// blow up of base locus
P31<X,Y,Z,T,u,v> := ProductProjectiveSpace(K,[3,1]);
Q_3 := (X + m*Y + a*Z)^2;
Q_2 := a^2*X^3 + X*Y*(b*X + c*Y) + m^2*Y^3 + (e*X^2 + f*X*Y + c*Y^2)*Z + (b*X + e*Y)*Z^2 + Z^3;
Q_1 := 2*a*X^3*Y + e*X^2*Y^2 + 2*a*m*X*Y^3 + (2*a*m*X^3 + f*X^2*Y + f*X*Y^2 + 2*m*Y^3)*Z + (c*X^2 + f*X*Y + b*Y^2)*Z^2 + 2*(m*X + a*Y)*Z^3;
Q_0 := X^3*Y^2 + a^2*X^2*Y^3 + X*Y*(2*m*X^2 + b*X*Y + 2*a*Y^2)*Z + (m^2*X^3 + c*X^2*Y + e*X*Y^2 + Y^3)*Z^2 + (m*X + a*Y)^2*Z^3;
Q := Q_3*T^3 + Q_2*T^2 + Q_1*T + Q_0;
BU := u*X*Z - v*Y*T;
M := JacobianMatrix([Q, BU]);
MXYZT := Submatrix(M, 1,1, 2,4);
eqs := Minors(MXYZT, 2);
CGBU := Scheme(P31, [Q, BU]);
SingC := Scheme(P31, [Q, BU] cat eqs);
