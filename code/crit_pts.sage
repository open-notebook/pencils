fig, ax = plt.subplots()
colours = cm.rainbow(np.linspace(0, 1, 14))
def rational_approx(beta, number_field_K, err=DEFAULT_ERROR):
    '''Returns a K-rational approximation of beta

    Here:
    - beta is an element of QQbar,
    - number_field_K is a number field
      *with a specified nonreal embedding into QQbar*.
    - err is the maximum error of the rational approximations 
      of the real and nonreal parts of beta.
    '''
    
    alpha = number_field_K._gens[0]
    re_beta, im_beta = beta.real(), beta.imag()
    re_alpha, im_alpha = QQbar(alpha).real(), QQbar(alpha).imag()
    x_coord = n((im_alpha * re_beta
                 - im_beta * re_alpha) / im_alpha).nearby_rational(err)
    y_coord = n(im_beta / im_alpha).nearby_rational(err)
    return x_coord + alpha * y_coord

# Ground field for CG surface
F = var('F')
minp = F^3+F^2-1
rs = [QQbar(rt_data[0]) for rt_data in minp.roots()]
NF.<r> = NumberField(minp, embedding = QQbar(rs[0])) # pick a complex root
basepoint = 2 + 0*r

def end_point(choice=0):
    '''Returns a QQ(r)-rational approximation to the chosen critical value:

    choice = 0 gives 0
             1 gives 1
             2,3,4,5 give roots of factor_1
             6,7,8,9,10,11,12,13 give roots of factor_2
             14 gives \infty
    '''
    
    if choice in [0,1]:
        return choice + 0 * r
    elif choice == 14:
        print("Currently we're just approximating" +
              "infinity by a big number (1000000)")
        return 1000000 + 0 * r
    else:
        # Pick critical points of bicanonical pencil
        NF_extender.<G> = PolynomialRing(NF)
        factor_1 = (G^4 - (5*r^2+14)*G^3
                    + (38*r^2+60*r+79)*G^2
                    - (5*r^2+14)*G+1)
        factor_2 = (59*G^8 + (51065*r^2-7308*r-24070)*G^7
                    + (1904132*r^2+2715049*r-3134629)*G^6
                    + (-9934542*r^2-11947505*r+14684907)*G^5
                    + (16297001*r^2+18575719*r-23300196)*G^4
                    + (-9934542*r^2-11947505*r+14684907)*G^3
                    + (1904132*r^2+2715049*r-3134629)*G^2
                    + (51065*r^2-7308*r-24070)*G + 59)
        if choice in [2,3,4,5]:
            mipol = factor_1
        else:
            mipol = factor_2
            
        NF_extended.<g_aux> = NumberField(mipol)
        ground_field = NF_extended.absolute_field('g')
        from_ext, to_ext = ground_field.structure()
        valid_embs = [ebd
                      for ebd in ground_field.embeddings(QQbar)
                      if abs(CDF(ebd(to_ext(r))) - CDF(rs[0])) < 0.1]
        exact_crit_vals = [ebd(to_ext(G))
                           for ebd in valid_embs]
        crit_vals = [rational_approx(cv, NF)
                     for cv in exact_crit_vals]
        if choice in [2,3,4,5]:
            return crit_vals[choice - 2]
        elif choice in [6,7,8,9,10,11,12,13]:
            return crit_vals[choice - 6]

for choice in range(0,14):
    plt_pt = CDF(end_point(choice))
    ax.scatter([plt_pt.real()], [plt_pt.imag()])
    
ax.scatter([2],[0],c='black')
ax.scatter([-2*rs[0].real()], [-2*rs[0].imag()], c='black')
plt.show()
