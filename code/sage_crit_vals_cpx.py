import matplotlib.pyplot as plt

# Ground field for CG surface
F = var('F')
minp = F^3+F^2-1
NF.<r> = NumberField(minp,embedding = QQbar(minp.roots()[0][0])) 

NF_extender.<G> = PolynomialRing(NF)
factor_1 = (G^4 - (5*r^2+14)*G^3
            + (38*r^2+60*r+79)*G^2
            - (5*r^2+14)*G+1)
factor_2 = (59*G^8 + (51065*r^2-7308*r-24070)*G^7
            + (1904132*r^2+2715049*r-3134629)*G^6
            + (-9934542*r^2-11947505*r+14684907)*G^5
            + (16297001*r^2+18575719*r-23300196)*G^4
            + (-9934542*r^2-11947505*r+14684907)*G^3
            + (1904132*r^2+2715049*r-3134629)*G^2
            + (51065*r^2-7308*r-24070)*G + 59)

rts = [sqrt(sqrt(rt)) for (rt,mul) in factor_1.roots(QQbar)]
rts += [sqrt(sqrt(rt)) for (rt,mul) in factor_2.roots(QQbar)]
rts += [0,1]
fig, ax = plt.subplots()
ax.scatter([rt.real() for rt in rts],[rt.imag() for rt in rts])
plt.show()
