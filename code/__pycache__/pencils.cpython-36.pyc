3
`�|]B  �               @   sB   d Z ddlmZ ddlZddlmZmZ dd� ZG dd� d�ZdS )	z3Classes for handling Lefschetz pencils with curver.�    )�copyN)�Surface�CoveringSpacec             C   s`   |dkr$| |kr$t | |�t| |�fS | | | dkr<|| fS ||  | dkrT| |fS td��dS )a?  Order two neighbouring points clockwise mod n.

    Given two neighbouring points in a clockwise cycle of length n,
    returns the same two points ordered clockwise. For example,

    order_clockwise(1,2,12) returns 1,2

    order_clockwise(1,12,12) returns 12,1

    This is a helper function for Hurwitz moves.

    �   �   z(Arguments are not cyclically consecutiveN)�min�max�	Exception)�i�j�n� r   �+/home/jde27/maths/pencils/python/pencils.py�order_clockwise
   s    r   c               @   s�   e Zd ZdZddd�Zdd� Zdd� Zd	d
� Zdd� Ze	dd� �Z
e	dd� �Ze	dd� �Ze	ddd��Ze	ddddgfdd��Ze	dd� �ZdS ) �PencilzoClass of Lefschetz pencils.

    Methods for finding global monodromy and implementing Hurwitz
    moves.

    �defaultc             C   s6   |dkr dd� t dt|��D �}|| _|| _|| _dS )a7  Create a Lefschetz pencil.

        Input: fibre surface; (cyclically) ordered list of vanishing
        cycles; (optionally) labels to remind you which vanishing
        cycles come from which critical points. Labels are useful for
        keeping track of curves through a sequence of Hurwitz moves.

        r   c             S   s   i | ]
}||�qS r   r   )�.0�xr   r   r   �
<dictcomp>3   s   z#Pencil.__init__.<locals>.<dictcomp>r   N)�range�len�fibre�vanishing_cycles�labels)�selfr   r   r   r   r   r   �__init__)   s    	zPencil.__init__c             C   s6   dd� | j D �}|jd�}x|r0||jd�9 }qW |S )z�Calculate global monodromy.

        The global monodromy is the ordered product of the Dehn twists
        in vanishing cycles.

        c             S   s   g | ]}|j � �qS r   )�encode_twist)r   Zvcr   r   r   �
<listcomp>@   s   z+Pencil.global_monodromy.<locals>.<listcomp>r   )r   �pop)r   Ztwists�global_monodromyr   r   r   r   9   s    

zPencil.global_monodromyc             C   s   | j � j� S )z/Check that the global monodromy relation holds.)r   Zis_identity)r   r   r   r   �check_relationG   s    zPencil.check_relationc             C   s�   | j |d  | j |d  f}t|t| j�f�� }t| j|d  �}t| j|d  �}||krp|j� |�| }}n||j� j� |� }}|| j|d < || j|d < | j |d  | j |d   | j |d < | j |d < dS )a�  Perform a Hurwitz move on a pencil.

        The data of a pencil comprises a (cyclically) ordered list of
        multi-curves on a surface. You should think of the following
        picture: you have a fibration over the plane (obtained by excising
        the fibre over infinity in our pencil); the fibre over the origin
        we will call T. The critical values are connected to the origin by
        straight line paths ('vanishing paths'). Over each vanishing path,
        you get a multi-curve ('vanishing cycle') in T, which collapses to
        the critical point as you move along the vanishing path.

        The global monodromy of a pencil is the product of Dehn twists in
        the vanishing cycles; if the critical values are (ordered
        clockwise) [v_1,...,v_n] then the global monodromy is
        tau_{v_1}...tau_{v_n}.

        Now you can start moving the critical values around. Hurwitz moves
        occur when the vanishing paths become collinear. This can occur in
        one of two ways:

        - Move (w,v): If w and v are cyclically ordered clockwise and
          w passes under v (closer to the origin) then you should
          replace the vanishing cycle for w by tau_w(v) and the
          vanishing cycle for v by w.

        - Move (v,w): If v passes under w then you should replace the
          vanishing cycle w by v and v by tau_v^{-1}(w).

        These two moves are called Hurwitz moves, and preserve the
        global monodromy:

        tau_{tau_w(v)} tau_w = tau_w tau_v tau_w^{-1} tau_w
                             = tau_w tau_v,

        tau_v tau_{tau_v^{-1}(w)} = tau_v tau_v^{-1} tau_w tau_v
                                  = tau_w tau_v.

        r   r   N)r   r   r   r   r   r   Zinverse)r   ZmoveZmove_indicesZclockZvc_0Zvc_1Zvc_0_newZvc_1_newr   r   r   �hurwitzK   s    (zPencil.hurwitzc                s�   � |�|kst �t|j� j� �}� �fdd���fdd�|D ���fdd�|D �}x4�fdd�|D �D ]}�|�}||krf|j|� qfW |S )z�Decompose into invariant submulticurves.

        Given an involution and an invariant multicurve, you get the
        maximal decomposition into invariant submulticurves.

        c                s<   | � | �kr| S dd� t | j� | �j�D �}�jj|�S dS )z=Take the orbit of a curve under the action of the involution.c             S   s   g | ]}|d  |d  �qS )r   r   r   )r   r   r   r   r   r   �   s   z:Pencil.split_multicurve.<locals>.orbit.<locals>.<listcomp>N)�zip�	geometricr   �
lamination)�curve�	new_curve)�
involutionr   r   r   �orbit�   s    z&Pencil.split_multicurve.<locals>.orbitc                s   i | ]}t � |�j� �|�qS r   )r   �
components)r   �cpt)r(   r   r   r   �   s   z+Pencil.split_multicurve.<locals>.<dictcomp>c                s   g | ]}� | d kr|�qS )r   r   )r   r*   )�
orbit_sizer   r   r   �   s   z+Pencil.split_multicurve.<locals>.<listcomp>c                s   g | ]}� | d kr|�qS )r   r   )r   r   )r+   r   r   r   �   s    )�AssertionError�listr)   �keys�append)r   �
multicurver'   r)   Z	splittingr*   Z
test_orbitr   )r'   r(   r+   r   r   �split_multicurve�   s    


zPencil.split_multicurvec                s~   dddgddd
gdddgdddgg}t |�� ddddddgddddddgg}|dd� td|�D �7 }� fd	d�|D �}� |fS )z�Return a collection of curves on the 4-punctured sphere.

        Unless degree==1, this is not a pencil in itself, but it is
        used to generate quadric pencils on Fermat surfaces of given
        degree.

        r   r   �   �   r   �   c          	   S   s4   g | ],}d | dd | d dd | d d | g�qS )r   r   r   )r   �pr   r   r   r   �   s   z"Pencil.lantern.<locals>.<listcomp>c                s   g | ]}� j |��qS r   )r$   )r   �c)�four_punctured_spherer   r   r   �   s   ���������������i��������������)r   r   )�cls�degree�	trianglesZcurves_as_lists�curvesr   )r7   r   �lantern�   s    	
zPencil.lanternc                sl   t j� �\}}� fdd�td� �D �}� fdd�}� fdd�}|j|||d����fd	d�|D �}| �|�S )
z8Return quadric pencil on Fermat surface of given degree.c                s$   g | ]}t d � �D ]}||f�qqS )r   )r   )r   r
   r   )r>   r   r   r   �   s   z!Pencil.fermat.<locals>.<listcomp>r   c                s   | d d �  | d fS )Nr   r   r   )r   )r>   r   r   �perm_0�   s    zPencil.fermat.<locals>.perm_0c                s   | d | d d �  fS )Nr   r   r   )r   )r>   r   r   �perm_5�   s    zPencil.fermat.<locals>.perm_5)r   r4   c                s   g | ]}� j |��qS r   )�preimage)r   r6   )�upstairsr   r   r   �   s   )r   rA   r   �	get_cover)r=   r>   �downstairs_surface�downstairs_curves�sheetsrB   rC   �upstairs_curvesr   )r>   rE   r   �fermat�   s    


zPencil.fermatc                sZ   t jd�\}}dddddg}dd� }d	d
� }|j|||d��� � fdd�|D �}| � |�S )z8Return bicanonical pencil on Fermat Z/5-Godeaux surface.r4   r   r   r   r3   r2   c             S   s   | d d S )Nr   r4   r   )r   r   r   r   rB   �   s    zPencil.godeaux.<locals>.perm_0c             S   s   | d d S )Nr   r4   r   )r   r   r   r   rC   �   s    zPencil.godeaux.<locals>.perm_5)r   r4   c                s   g | ]}� j |��qS r   )rD   )r   r6   )rE   r   r   r   �   s   z"Pencil.godeaux.<locals>.<listcomp>)r   rA   rF   )r=   rG   rH   rI   rB   rC   rJ   r   )rE   r   �godeaux�   s    

zPencil.godeauxr   r   c                 s�  t j� ��j}�j� |j|ddi��� ��fdd�tdd�D �}ddd�}|jdd	� tdd
�D �� ddd�}	� d � d |d d |d | |d |	|  |d | |d |	|  |d d |d | |d d |d |	|  |d | |d |	|  |d d |d | |d |	|  |d d g}
| ||
|��dddddd d!d"d#d$d%d&d'd(d)d*d+d,d-d.d/d0d1d2d3d4d5d6d7d8d9g}x|�r��j|jd�� �qxW �fdd�d:D �}|d |d k�o�|d |d k}�|fS );a+  Return Lefschetz pencil and Boolean.

        Pencil is bicanonical pencil on determinantal Z/5-Godeaux surface.

        The Boolean (which should be true) tells you if you have the
        correct matching cycles.

        As you deform the Fermat Godeaux to a determinantal Godeaux
        surface, the critical points of the bicanonical pencil move
        away from their very symmetric configuration; the 7 critical
        values split into 17 and these start wandering around the
        plane. This yields a sequence of Hurwitz moves, at the end of
        which four pairs of critical points collide to form nodes. In
        practice, that means that after this sequence of Hurwitz
        moves, the corresponding pairs of vanishing cycles become
        isotopic in their pairs.

        It is a little delicate to determine how the critical points
        split up over the 17 critical values. There are 32 ways in
        which this could happen: each quintuple of critical points
        turns into two pairs and a singlet; the pairs are orbits of a
        particular automorphism of the surface (called sigma); we
        therefore have to choose, in each of the five quintuples,
        which sigma-orbit is which, giving 2^5 possibilities.

        Given numbers a, b, c, d, e in {1, 2} this function picks a
        way to assign sigma-orbits of vanishing cycles to critical
        points in the pencil after deformation, and performs the
        desired sequence of Hurwitz moves. It returns the resulting
        pencil together with a Boolean which represents the truth or
        falsity of the proposition that the relevant pairs of
        vanishing cycles match up.

        This proposition is true only for the choice
        a, b, c, d, e = 2, 2, 1, 2, 1 (the default values of the arguments).

        r   c                s   g | ]}�j � | ���qS r   )r1   )r   r
   )r@   �fermat_godeaux�sigmar   r   r     s   z(Pencil.nodal_godeaux.<locals>.<listcomp>r   �   r   )r   �inftyc             S   s   i | ]}|d  |�qS )r   r   )r   r   r   r   r   r     s    z(Pencil.nodal_godeaux.<locals>.<dictcomp>�   )r   r   r3   r2   �   �   �   �   �   �
   �	   rP   r4   �   �   c                s   g | ]}� j � j|  �qS r   )r   r   )r   r
   )�split_pencilr   r   r   ,  s   r8   )r   rR   )r   rS   )r   rT   )r   rU   )r   rV   )r   rW   )r   rX   )rW   rV   )rT   rS   )rT   rR   )rS   rR   )rT   rP   )rT   r   )r   rT   )rR   rU   )rR   rW   )r3   r2   )r3   r4   )r3   rY   )r3   rO   )rY   rO   )rO   r4   )r2   rO   )r2   r4   )r2   rY   )r2   r3   )r2   rZ   )r2   r   )r2   rX   )r2   rV   )r2   rR   )r2   rW   rO   rT   )	r   rL   r   r   �find_isometryr   �updater!   r   )r=   �a�br6   �d�er   Z	split_vcsZsplit_labels�otherZsplit_curvesZ	move_list�matches�claimr   )r@   rM   rN   r[   r   �nodal_godeaux�   sD    '





"zPencil.nodal_godeauxTFc       
         s  | j � \}}|st�dd� |jD �}dd� }dd� }||d�}||d  ||d  ||d	  ||d	  ||d
  ||d
  ||d  ||d  ||d |d	 k ||d |d	 k |d�}|jjdd	g|���fdd�|D �}	dd� |	D �� ddddg�| �� �fdd�tdt� ��D ��S )a<  Return bicanonical pencil on double cover of nodal Godeaux surface.

        Take the double cover of the determinantal nodal Godeaux
        surface branched along the nodes. This surface has bicanonical
        pencil with fibres of genus 7.

        This function returns the corresponding bicanonical pencil;
        the optional argument 'choices' lets you change the double
        cover; the only 'choice' which works (so that the matching
        cycles for the nodes are not contained in the subgroup
        associated to the cover) is the default one.

        c             S   s   g | ]}|j � D ]}|�qqS r   )r)   )r   r0   r*   r   r   r   r   D  s   z8Pencil.double_cover_of_nodal_godeaux.<locals>.<listcomp>c             S   s   | d d S )Nr   r   r   )r   r   r   r   �nontrivI  s    z5Pencil.double_cover_of_nodal_godeaux.<locals>.nontrivc             S   s   | S )Nr   )r   r   r   r   �trivK  s    z2Pencil.double_cover_of_nodal_godeaux.<locals>.triv)TFr   r   r   r3   )r   �   rO   �   rY   �   rU   �   rT   rR   r   c                s   g | ]}� j |��qS r   )rD   )r   r6   )�cover_of_nodalr   r   r   Y  s    c             S   s   g | ]}|j � D ]}|�qqS r   )r)   )r   r0   r*   r   r   r   r   Z  s   rZ   rX   �$   �%   c                s   g | ]}|�kr� | �qS r   r   )r   r   )�components_upstairs�	droppingsr   r   r   b  s   )re   r,   r   r   rF   r   r   )
r=   ZchoicesZnodalrd   r@   rf   rg   ZmonodZpermsZcurves_upstairsr   )ro   rl   rp   r   �double_cover_of_nodal_godeaux2  s.    

z$Pencil.double_cover_of_nodal_godeauxc                s�  | j � }|j}t|j�}|j|ddi�|j|ddi�g�g � xV|jD ]L��fdd��D �}|�fdd��D �7 }t� fdd�|D ��rD� j�� qDW � �fdd���fd	d�|jD �}g }x^|D ]V��fd
d�|D �}�fdd�|D �}�fdd�|D �}	| r�| r�|	 r�|j�� q�W t|�}
g }x�|j	D ]���d ���k�rt|
j
� �fdd�td|�D ��}|j||g� nF�d ���� ��fdd�td|�D �}|
j
|�}||k�r.||g7 }�q.W | |
|�S )z4Return the bicanonical pencil on the Barlow surface.r   �   c                s   g | ]}|j �  �qS r   )�	label_map)r   �g)�edger   r   r   p  s    z!Pencil.barlow.<locals>.<listcomp>c                s   g | ]}|j �   �qS r   )rs   )r   rt   )ru   r   r   r   q  s    c                s   g | ]}|� k�qS r   r   )r   �o)�barlow_edgesr   r   r   r  s    c                sn   � fdd��D �}� fdd��D �}�fdd�|D �}�fdd�|D �}|rZ�j |d �S �j |d � S d S )Nc                s   g | ]}|j �  �qS r   )rs   )r   rt   )ru   r   r   r   v  s    z7Pencil.barlow.<locals>.edge_conv_fn.<locals>.<listcomp>c                s   g | ]}|j �   �qS r   )rs   )r   rt   )ru   r   r   r   w  s    c                s   g | ]}|� kr|�qS r   r   )r   rv   )rw   r   r   r   x  s    c                s   g | ]}|� kr|�qS r   r   )r   rv   )rw   r   r   r   y  s    r   )�index)ru   Z
orbit_plusZorbit_minusZrep_plusZ	rep_minus)rw   �
isometries)ru   r   �edge_conv_fnu  s    z#Pencil.barlow.<locals>.edge_conv_fnc                s   g | ]}� fd d�|D ��qS )c                s   g | ]}� |��qS r   r   )r   ru   )rz   r   r   r     s    z,Pencil.barlow.<locals>.<listcomp>.<listcomp>r   )r   �tri)rz   r   r   r     s   c                s   g | ]}|� kr|�qS r   r   )r   r   )r{   r   r   r   �  s    c                s*   g | ]"}|� d  � d � d gkr|�qS )r   r   r   r   )r   r   )r{   r   r   r   �  s    c                s*   g | ]"}|� d  � d � d gkr|�qS )r   r   r   r   )r   r   )r{   r   r   r   �  s    r   c                s   g | ]}|� kr�j | �qS r   )r#   )r   r
   )rw   r%   r   r   r   �  s   c                s(   g | ] }|� kr�j | �j |  �qS r   )r#   )r   r
   )rw   r%   �image_curver   r   r   �  s   i����)rq   r   r   �indicesr\   �allr/   r?   r   r   r$   r   �extend)r=   Zdouble_coverZgenus_7r   r(   Znew_trianglesZbarlow_trianglesZtri1Ztri2Ztri3Zbarlow_fibreZbarlow_curvesr&   Znew_curve_as_listr   )rw   r%   ru   rz   r|   ry   r{   r   �barlowf  sF    






zPencil.barlowN)r   )r   r   r   r   r   )�__name__�
__module__�__qualname__�__doc__r   r   r    r!   r1   �classmethodrA   rK   rL   re   rq   r�   r   r   r   r   r   !   s   
7$Q3r   )r�   r   ZcurverZcovering_spacesr   r   r   r   r   r   r   r   �<module>   s
   