"""Classes for handling Lefschetz pencils with curver."""

from copy import copy

import curver

from covering_spaces import Surface, CoveringSpace


def order_clockwise(i, j, n):
    """Order two neighbouring points clockwise mod n.

    Given two neighbouring points in a clockwise cycle of length n,
    returns the same two points ordered clockwise. For example,

    order_clockwise(1,2,12) returns 1,2

    order_clockwise(1,12,12) returns 12,1

    This is a helper function for Hurwitz moves.

    """
    if n == 2 and i != j:
        return min(i, j), max(i, j)
    if (i-j) % n == 1:
        return j, i
    elif (j-i) % n == 1:
        return i, j
    else:
        raise Exception('Arguments are not cyclically consecutive')


class Pencil:
    """Class of Lefschetz pencils.

    Methods for finding global monodromy and implementing Hurwitz
    moves.

    """

    def __init__(self, fibre, vanishing_cycles, labels='default'):
        """Create a Lefschetz pencil.

        Input: fibre surface; (cyclically) ordered list of vanishing
        cycles; (optionally) labels to remind you which vanishing
        cycles come from which critical points. Labels are useful for
        keeping track of curves through a sequence of Hurwitz moves.

        """
        if labels == 'default':
            labels = {x: x
                      for x in range(0, len(vanishing_cycles))}
        self.fibre = fibre
        self.vanishing_cycles = vanishing_cycles
        self.labels = labels

    def global_monodromy(self):
        """Calculate global monodromy.

        The global monodromy is the ordered product of the Dehn twists
        in vanishing cycles.

        """
        twists = [vc.encode_twist()
                  for vc in self.vanishing_cycles]
        global_monodromy = twists.pop(0)
        while twists:
            global_monodromy *= twists.pop(0)
        return global_monodromy

    def check_relation(self):
        """Check that the global monodromy relation holds."""
        return self.global_monodromy().is_identity()

    def hurwitz(self, move):
        """Perform a Hurwitz move on a pencil.

        The data of a pencil comprises a (cyclically) ordered list of
        multi-curves on a surface. You should think of the following
        picture: you have a fibration over the plane (obtained by excising
        the fibre over infinity in our pencil); the fibre over the origin
        we will call T. The critical values are connected to the origin by
        straight line paths ('vanishing paths'). Over each vanishing path,
        you get a multi-curve ('vanishing cycle') in T, which collapses to
        the critical point as you move along the vanishing path.

        The global monodromy of a pencil is the product of Dehn twists in
        the vanishing cycles; if the critical values are (ordered
        clockwise) [v_1,...,v_n] then the global monodromy is
        tau_{v_1}...tau_{v_n}.

        Now you can start moving the critical values around. Hurwitz moves
        occur when the vanishing paths become collinear. This can occur in
        one of two ways:

        - Move (w,v): If w and v are cyclically ordered clockwise and
          w passes under v (closer to the origin) then you should
          replace the vanishing cycle for w by tau_w(v) and the
          vanishing cycle for v by w.

        - Move (v,w): If v passes under w then you should replace the
          vanishing cycle w by v and v by tau_v^{-1}(w).

        These two moves are called Hurwitz moves, and preserve the
        global monodromy:

        tau_{tau_w(v)} tau_w = tau_w tau_v tau_w^{-1} tau_w
                             = tau_w tau_v,

        tau_v tau_{tau_v^{-1}(w)} = tau_v tau_v^{-1} tau_w tau_v
                                  = tau_w tau_v.

        """
        # Determine whether we're doing move (w,v) or (v,w)
        move_indices = (self.labels[move[0]], self.labels[move[1]])
        clock = order_clockwise(*move_indices, len(self.vanishing_cycles))
        vc_0 = copy(self.vanishing_cycles[clock[0]])
        vc_1 = copy(self.vanishing_cycles[clock[1]])
        if clock == move_indices:
            vc_0_new, vc_1_new = (vc_0.encode_twist())(vc_1), vc_0
        else:
            vc_0_new, vc_1_new = vc_1, ((vc_1.encode_twist()).inverse())(vc_0)

        self.vanishing_cycles[clock[0]] = vc_0_new
        self.vanishing_cycles[clock[1]] = vc_1_new

        (self.labels[move[0]], self.labels[move[1]]) = (self.labels[move[1]],
                                                        self.labels[move[0]])

    def split_multicurve(self, multicurve, involution):
        """Decompose into invariant submulticurves.

        Given an involution and an invariant multicurve, you get the
        maximal decomposition into invariant submulticurves.

        """
        assert involution(multicurve) == multicurve
        components = list(multicurve.components().keys())

        def orbit(curve):
            """Take the orbit of a curve under the action of the involution."""
            if curve == involution(curve):
                return curve
            else:
                new_curve = [x[0]+x[1]
                             for x in zip(curve.geometric,
                                          involution(curve).geometric)]
                return self.fibre.lamination(new_curve)

        orbit_size = {cpt: len(orbit(cpt).components())
                      for cpt in components}

        # First, list all components whose orbit is connected
        splitting = [cpt
                     for cpt in components
                     if orbit_size[cpt] == 1]
        # Then add in the components whose orbit is disconnected
        # (without repeats)
        for cpt in [x for x in components if orbit_size[x] == 2]:
            test_orbit = orbit(cpt)
            if test_orbit not in splitting:
                splitting.append(test_orbit)

        return splitting

    @classmethod
    def lantern(cls, degree):
        """Return a collection of curves on the 4-punctured sphere.

        Unless degree==1, this is not a pencil in itself, but it is
        used to generate quadric pencils on Fermat surfaces of given
        degree.

        """
        triangles = [[0, 1, 4],
                     [3, 2, ~0],
                     [~1, ~2, 5],
                     [~5, ~3, ~4]]
        four_punctured_sphere = Surface(triangles)
        curves_as_lists = [[1, 0, 1, 0, 1, 1],
                           [1, 1, 0, 1, 0, 1]]
        curves_as_lists += [[2*p, 1, 2*p+1, 1, 2*p+1, 2*p]
                            for p in range(0, degree)]
        curves = [four_punctured_sphere.lamination(c)
                  for c in curves_as_lists]
        return four_punctured_sphere, curves

    @classmethod
    def fermat(cls, degree):
        """Return quadric pencil on Fermat surface of given degree."""
        downstairs_surface, downstairs_curves = Pencil.lantern(degree)
        sheets = [(i, j)
                  for i in range(0, degree)
                  for j in range(0, degree)]

        def perm_0(x): return ((x[0] + 1) % degree, x[1])

        def perm_5(x): return (x[0], (x[1] + 1) % degree)

        upstairs = downstairs_surface.get_cover(sheets,
                                                {0: perm_0,
                                                 5: perm_5})
        upstairs_curves = [upstairs.preimage(c)
                           for c in downstairs_curves]
        return cls(upstairs, upstairs_curves)

    @classmethod
    def godeaux(cls):
        """Return bicanonical pencil on Fermat Z/5-Godeaux surface."""
        downstairs_surface, downstairs_curves = Pencil.lantern(5)
        sheets = [0, 1, 2, 3, 4]

        def perm_0(x): return (x + 1) % 5

        def perm_5(x): return (x + 2) % 5

        upstairs = downstairs_surface.get_cover(sheets,
                                                {0: perm_0,
                                                 5: perm_5})
        upstairs_curves = [upstairs.preimage(c)
                           for c in downstairs_curves]
        return cls(upstairs, upstairs_curves)

    @classmethod
    def nodal_godeaux(cls, a=2, b=2, c=1, d=2, e=1):
        """Return Lefschetz pencil and Boolean.

        Pencil is bicanonical pencil on determinantal Z/5-Godeaux surface.

        The Boolean (which should be true) tells you if you have the
        correct matching cycles.

        As you deform the Fermat Godeaux to a determinantal Godeaux
        surface, the critical points of the bicanonical pencil move
        away from their very symmetric configuration; the 7 critical
        values split into 17 and these start wandering around the
        plane. This yields a sequence of Hurwitz moves, at the end of
        which four pairs of critical points collide to form nodes. In
        practice, that means that after this sequence of Hurwitz
        moves, the corresponding pairs of vanishing cycles become
        isotopic in their pairs.

        It is a little delicate to determine how the critical points
        split up over the 17 critical values. There are 32 ways in
        which this could happen: each quintuple of critical points
        turns into two pairs and a singlet; the pairs are orbits of a
        particular automorphism of the surface (called sigma); we
        therefore have to choose, in each of the five quintuples,
        which sigma-orbit is which, giving 2^5 possibilities.

        Given numbers a, b, c, d, e in {1, 2} this function picks a
        way to assign sigma-orbits of vanishing cycles to critical
        points in the pencil after deformation, and performs the
        desired sequence of Hurwitz moves. It returns the resulting
        pencil together with a Boolean which represents the truth or
        falsity of the proposition that the relevant pairs of
        vanishing cycles match up.

        This proposition is true only for the choice
        a, b, c, d, e = 2, 2, 1, 2, 1 (the default values of the arguments).

        """
        fermat_godeaux = Pencil.godeaux()
        fibre = fermat_godeaux.fibre
        curves = fermat_godeaux.vanishing_cycles

        # Split the vanishing cycles up as the critical values move
        sigma = fibre.find_isometry(fibre, {0: ~0})
        split_vcs = [fermat_godeaux.split_multicurve(curves[i], sigma)
                     for i in range(2, 7)]
        split_labels = {0: 0, 'infty': 1}
        split_labels.update({x: x+1 for x in range(1, 16)})
        other = {1: 2, 2: 1}
        split_curves = [curves[0], curves[1],
                        split_vcs[0][0], split_vcs[0][a],
                        split_vcs[0][other[a]],
                        split_vcs[1][b], split_vcs[1][other[b]],
                        split_vcs[1][0],
                        split_vcs[2][c], split_vcs[2][0],
                        split_vcs[2][other[c]],
                        split_vcs[3][d], split_vcs[3][other[d]],
                        split_vcs[3][0],
                        split_vcs[4][e], split_vcs[4][other[e]],
                        split_vcs[4][0]]
        split_pencil = cls(fibre, split_curves, split_labels)

        # Perform a sequence of Hurwitz moves to get to the
        # determinantal Godeaux
        move_list = [(0, 15), (0, 14), (0, 13), (0, 12), (0, 11),
                     (0, 10), (0, 9), (10, 11),
                     (13, 14), (13, 15), (14, 15), (13, 'infty'),
                     (13, 1), (2, 13), (15, 12),
                     (15, 10), (3, 4), (3, 5), (3, 6), (3, 7), (6, 7),
                     (7, 5), (4, 7), (4, 5), (4, 6), (4, 3), (4, 8),
                     (4, 0), (4, 9), (4, 11), (4, 15)]
        while move_list:
            split_pencil.hurwitz(move_list.pop(0))

        # Find out whether our matching cycles match
        matches = [split_pencil.vanishing_cycles[split_pencil.labels[i]]
                   for i in [4, 10, 7, 13]]
        claim = (matches[0] == matches[1] and matches[2] == matches[3])

        return split_pencil, claim

    @classmethod
    def double_cover_of_nodal_godeaux(cls, choices=[True, False, True, False]):
        """Return bicanonical pencil on double cover of nodal Godeaux surface.

        Take the double cover of the determinantal nodal Godeaux
        surface branched along the nodes. This surface has bicanonical
        pencil with fibres of genus 7.

        This function returns the corresponding bicanonical pencil;
        the optional argument 'choices' lets you change the double
        cover; the only 'choice' which works (so that the matching
        cycles for the nodes are not contained in the subgroup
        associated to the cover) is the default one.

        """
        nodal, claim = cls.nodal_godeaux()
        assert(claim)
        # Separate all multicurves into their components
        curves = [cpt
                  for multicurve in nodal.vanishing_cycles
                  for cpt in multicurve.components()]

        # Set up monodromies for double cover
        def nontriv(x): return (x + 1) % 2

        def triv(x): return x

        monod = {True: nontriv, False: triv}
        perms = {1: monod[choices[0]], 27: monod[choices[0]],
                 7: monod[choices[1]], 21: monod[choices[1]],
                 6: monod[choices[2]], 24: monod[choices[2]],
                 12: monod[choices[3]], 18: monod[choices[3]],
                 13: monod[choices[0] != choices[1]],
                 15: monod[choices[0] != choices[1]],
                 0: triv}

        # Take double cover
        cover_of_nodal = nodal.fibre.get_cover([0, 1],
                                               perms)
        curves_upstairs = [cover_of_nodal.preimage(c) for c in curves]
        components_upstairs = [cpt
                               for multicurve in curves_upstairs
                               for cpt in multicurve.components()]

        # Drop those curves which we are now overcounting
        droppings = [8, 9, 36, 37]

        return cls(cover_of_nodal,
                   [components_upstairs[x]
                    for x in range(0, len(components_upstairs))
                    if x not in droppings])

    @classmethod
    def barlow(cls):
        """Return the bicanonical pencil on the Barlow surface."""
        double_cover = cls.double_cover_of_nodal_godeaux()
        genus_7 = double_cover.fibre
        n = len(genus_7.indices)
        isometries = [genus_7.find_isometry(genus_7, {0: 0}),
                      genus_7.find_isometry(genus_7, {0: ~30})]
        barlow_edges = []
        for edge in genus_7.indices:
            orbit = [g.label_map[edge] for g in isometries]
            orbit += [g.label_map[~edge] for g in isometries]
            if all([o not in barlow_edges for o in orbit]):
                barlow_edges.append(edge)

        def edge_conv_fn(edge):
            orbit_plus = [g.label_map[edge] for g in isometries]
            orbit_minus = [g.label_map[~edge] for g in isometries]
            rep_plus = [o for o in orbit_plus if o in barlow_edges]
            rep_minus = [o for o in orbit_minus if o in barlow_edges]
            if rep_plus:
                return barlow_edges.index(rep_plus[0])
            else:
                return ~barlow_edges.index(rep_minus[0])

        new_triangles = [[edge_conv_fn(edge) for edge in tri]
                         for tri in genus_7.triangles]

        # eliminate repeated triangles
        barlow_triangles = []
        for tri in new_triangles:
            tri1 = [x for x in barlow_triangles
                    if x == tri]
            tri2 = [x for x in barlow_triangles
                    if x == [tri[1], tri[2], tri[0]]]
            tri3 = [x for x in barlow_triangles
                    if x == [tri[2], tri[0], tri[1]]]
            if not tri1 and not tri2 and not tri3:
                barlow_triangles.append(tri)

        barlow_fibre = Surface(barlow_triangles)

        barlow_curves = []
        for curve in double_cover.vanishing_cycles:
            if isometries[1](curve) == curve:
                new_curve = barlow_fibre.lamination([curve.geometric[i]
                                                     for i in range(0, n)
                                                     if i in barlow_edges])
                barlow_curves.extend([new_curve, new_curve])
            else:
                image_curve = isometries[1](curve)
                new_curve_as_list = [curve.geometric[i] +
                                     image_curve.geometric[i]
                                     for i in range(0, n)
                                     if i in barlow_edges]
                new_curve = barlow_fibre.lamination(new_curve_as_list)
                if new_curve not in barlow_curves:
                    barlow_curves += [new_curve]

        return cls(barlow_fibre, barlow_curves)
