#!/usr/bin/python3

import curver
from functools import reduce 

def compos(lst):
    '''Multiply elements of a list'''
    return reduce((lambda x,y: x*y),lst)

def multi_twist(lam):
    '''Given a lamination representing
    a disjoint union of simple closed
    curves, this returns the product
    of Dehn twists in these curves.'''

    if lam.is_multicurve():
        cpnts=lam.components().keys()
        twist_list=[cpnt.encode_twist()
                    for cpnt in cpnts]
        return compos(twist_list)
    else:
        raise Exception('Lamination is not\
                        a disjoint union of\
                        simple closed curves')

def converter(edge_list):
    '''Input: a list of edge labels
       Output: a function which takes a pair (x,y)
               with x in {0,1} and y in edge_list
               and returns the index of y in edge_list
               if x==0 (or ~index if x==1)

    curver requires edges to be labelled by
    consecutive integers. We want more general
    labels. x denotes the orientation of the edge.'''
    def conv_func(tuplet):
        if tuplet[0]==0:
            return edge_list.index(tuplet[1])
        else:
            return ~edge_list.index(tuplet[1])
    return conv_func

def curve_gen(T,edge_list,patt_conv):
    '''Given a triangulated surface, an edge_list,
    and a function edge_list --> indices of patt
    which tells us how to colour the edges with the
    pattern, this returns a function which generates
    a curve from a pattern of intersections.

    For example, if:
    - edge_list=[(0,0),(0,1),(1,0),(1,1)],
    - the pattern has length 2
    - and patt_conv(x,y)=y
    then we get the function which, when fed a
    list [a,b], yields the curve whose intersection
    with edge (x,y) is a if y=0 and b if y=1.

    '''
    def curve_from_pattern(patt):
        intersections=[0 for i in edge_list]
        for edge in edge_list:
            m=edge_list.index(edge)
            intersections[m]=patt[patt_conv(edge)]
        return T.lamination(intersections)
    return curve_from_pattern

def covered_relation(n):
    '''Returns the triangles, patterns, edge list
    and pattern_converter for the relation coming
    from the quadric pencil [x_1x_4:x_2x_3] on a
    Fermat surface \sum_{i=1}^4 x_i^n=0.

    We choose to represent the oriented arc k_{ij}
    in the ideal triangulation as (x,(i,j,k)) with
    x in {0,1} telling us the orientation
    (k in {0,...,5}, i,j in {0,...,n-1} specifying
    the arc)

    An oriented triangle is a triple of
    oriented edges.

    The curves we're interested in have the property
    that the geometric intersection with k_{ij} is
    independent of i,j, so our patterns are of length
    6 (telling us how curves hit 0_{ij},...,5_{ij}).
    In particular, the pattern_converter(i,j,k)=k.

    '''

    edge_list=[(i,j,k)
               for i in range(0,n)
               for j in range(0,n)
               for k in range(0,6)]
    pattern_converter=lambda tripl: tripl[2]
    
    A_tri=[[(1,(i,j,4)),(1,(i,j,1)),(0,(i,j,0))]
           for i in range(0,n)
           for j in range(0,n)]
    B_tri=[[(1,(i,(j+1)%n,0)),(1,(i,j,2)),(1,(i,j,3))]
           for i in range(0,n)
           for j in range(0,n)]
    C_tri=[[(0,(i,j,5)),(0,(i,j,2)),(0,(i,j,1))]
           for i in range(0,n)
           for j in range(0,n)]
    D_tri=[[(0,(i,j,4)),(0,(i,j,3)),(1,((i+1)%n,j,5))]
           for i in range(0,n)
           for j in range(0,n)]
    raw_triangles=A_tri+B_tri+C_tri+D_tri

    patterns=[[1,1,0,1,0,1],[1,0,1,0,1,1]]
    patterns.extend([[2*p,2*p+1,1,2*p+1,1,2*p]
                     for p in range(0,n)])

    return edge_list,pattern_converter,\
        raw_triangles,patterns

def z5_godeaux():
    n=5
    edge_list=[(j,k)
               for j in range(0,n)
               for k in range(0,6)]
    pattern_converter=lambda tripl: tripl[1]
    gluing={1:4,4:2,2:0,0:3,3:1}
    g=lambda a: gluing[a]

    A_tri=[[(1,(j,4)),(1,(j,1)),(0,(j,0))]
           for j in range(0,n)]
    B_tri=[[(1,((j+1)%n,0)),(1,(j,2)),(1,(j,3))]
           for j in range(0,n)]
    C_tri=[[(0,(i,5)),(0,(i,2)),(0,(i,1))]
           for i in range(0,n)]
    D_tri=[[(0,(i,4)),(0,(i,3)),(1,(g(i),5))]
           for i in range(0,n)]
    raw_triangles=A_tri+B_tri+C_tri+D_tri
    
    patterns=[[1,1,0,1,0,1],[1,0,1,0,1,1]]
    patterns.extend([[2*p,2*p+1,1,2*p+1,1,2*p]
                     for p in range(0,n)])
    return edge_list,pattern_converter,\
        raw_triangles,patterns

def check_relation(edge_list,
                   pattern_converter,
                   raw_triangles,
                   patterns):
    edge_converter=converter(edge_list)
    triangle_converter=lambda tri:(
        list(map(edge_converter,tri)))
    triangles=[triangle_converter(tri)
               for tri in raw_triangles]
    T=curver.create_triangulation(triangles)
    curve_from_pattern=curve_gen(T,
                                 edge_list,
                                 pattern_converter)
    t=[multi_twist(curve_from_pattern(patterns[x]))
       for x in range(0,len(patterns))]
    return compos(t).is_identity()


def godeaux_homology_calc(edge_list,
                          pattern_converter,
                          raw_triangles,
                          patterns):
    edge_converter=converter(edge_list)
    triangle_converter=lambda tri:(
        list(map(edge_converter,tri)))
    triangles=[triangle_converter(tri)
               for tri in raw_triangles]
    T=curver.create_triangulation(triangles)
    curve_from_pattern=curve_gen(T,
                                 edge_list,
                                 pattern_converter)
    hitlists=[[(0,1),(0,2),(0,3),(0,4)],
              [(1,1),(1,2),(1,3),(1,4)],
              [(3,1),(3,2),(3,3),(3,4)],
              [(4,1),(4,2),(4,3),(4,4)],
              [(1,0),(1,4),(4,5),(4,1),(4,4),(2,5),(2,1),(2,4),(0,5),(0,2)],
              [(2,0),(2,1),(2,5),(4,4),(4,1),(4,5),(1,3)],
              [(3,0),(2,3),(0,5),(0,2),(0,3),(3,5),(3,1)],                    
              [(4,0),(4,4),(2,5),(2,2),(2,3),(0,5),(0,2),(0,3),(3,5),(3,2)]]
    intlists=[[hitlist.count(k)
               for k in edge_list]
              for hitlist in hitlists]
    homology_basis=[T.lamination(intlist)
                    for intlist in intlists]
    multicurves=[curve_from_pattern(patterns[x])
                 for x in range(0,len(patterns))]
    curves={m:list(m.components().keys())
            for m in multicurves}

    def int_to_cls(lst):
        clsZ=[lst[4]-lst[5],lst[5],lst[6],lst[7]-lst[6],
              lst[0],lst[1]-lst[0],lst[2]-lst[3],lst[3]]
        return [i%2 for i in clsZ]

    def monodr(lst):
        monZ=[lst[0]+lst[3],lst[1]+lst[2],lst[4]+lst[7],lst[5]+lst[6]]
        return [i%2 for i in monZ]
    
    curve_classes={m:[monodr(int_to_cls([c.intersection(b)
                                         for b in homology_basis]))
                      for c in curves[m]]
                   for m in multicurves}
    #cntr=0
    #for m in curve_classes:
    #    print('\n Multicurve '+str(cntr)+'\n')
    #    cntr=cntr+1
    #    for x in curve_classes[m]:
    #        print(str(x)+'\n')
    monodromies=[[X,Y,Z,W]
                 for X in [0,1]
                 for Y in [0,1]
                 for Z in [0,1]
                 for W in [0,1]]
    for mon in monodromies:
        def monfn(lst):
            return sum([zp[0]*zp[1] for zp in zip(mon,lst)])%2
        biglist=[]
        for m in curve_classes:
            for x in curve_classes[m]:
                biglist+=[monfn(x)]
        if sum(biglist)==8:
            print('Monodromy '+str(mon)+' : '+str(biglist))


#assert check_relation(*covered_relation(2))
#assert check_relation(*z5_godeaux())
godeaux_homology_calc(*z5_godeaux())
