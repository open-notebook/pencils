'''Functions for use with the curver package.

The most useful ones are check_relation and make_pencil'''

import curver
from functools import reduce 
from gauss import gauss

def compos(lst):
    '''Returns the product of elements in a list.'''

    return reduce((lambda x,y: x*y),lst)

def multi_twist(lam):
    '''Returns the product of Dehn twists in the components of a
    multicurve.'''
    
    if lam.is_multicurve():
        cpnts=lam.components().keys()
        twist_list=[cpnt.encode_twist()
                    for cpnt in cpnts]
        return compos(twist_list)
    else:
        raise Exception('Lamination is not\
                        a disjoint union of\
                        simple closed curves')


def curve_to_dual(curve,basis):
    '''Given a curve C and a basis e_1,...,e_n for homology, returns the
    vector <C| := ([C].e_1,...,[C].e_n)
    
    '''
    return [curve.intersection(b) for b in basis]

def dual_curve_to_homology_class(lst,basis):
    '''Given a vector v and a Z/2-basis e_1,...,e_n for the homology,
    finds the Z/2-homology class of the curve C such that v_i =
    <C|e_i>.

    '''
    A=[curve_to_dual(b,basis) for b in basis] # note that this is a
                                              # symmetric matrix
    gauss_prob=[A[i]+[lst[i]]
                for i in range(0,len(A))]
    # solve the dual linear algebra problem <C| = \sum c_i <e_i|
    return gauss(gauss_prob)

def homology_class(input_curve,basis):
    '''Given a curve C and a basis e_1,...,e_n for homology, returns the
    Z/2-vector of coefficients c_i such that [C]=\sum c_i e_i.

    '''
    lst=curve_to_dual(input_curve,basis)
    return dual_curve_to_homology_class(lst,basis)
    
def check_relation(T,curves,*args):
    '''Checks that a relation holds in the mapping class group'''
    
    t=[curve.encode_twist() for curve in curves]
    return compos(t).is_identity()
                   

def converter(edge_list):
    '''
    Given a list of labels for edges, converter returns a function
    which converts an oriented label into an oriented curver label
    (i.e. an integer in {0,..,N} together with a possible
    tilde). Oriented labels are assumed to be pairs (i,a) where i\in
    {0,1}.

    '''

    def conv_func(tuplet):
        if tuplet[0]==0:
            return edge_list.index(tuplet[1])
        else:
            return ~edge_list.index(tuplet[1])
    return conv_func

def get_curve_from_hitlist(T,edge_list):
    '''Returns a function which generates a simple closed curve from the
    list of edges it hits in the order it hits them (i.e. possibly
    with repeats). Assumes that the curve is in minimal position with
    respect to each edge (no bigons).

    '''
    def curve_from_hitlist(hitlist):
        intlist=[hitlist.count(k)
                 for k in edge_list]
        return T.lamination(intlist)

    return curve_from_hitlist


def get_curve_from_pattern(T,edge_list,patt_conv):
    '''Returns a function which takes a pattern (i.e. a list of integers)
    and returns the curver.Lamination whose geometric intersection
    with edge x equals pattern(patt_conv(x))

    '''
    def curve_from_pattern(patt):
        intersections=[0 for i in edge_list]
        for edge in edge_list:
            m=edge_list.index(edge)
            intersections[m]=patt[patt_conv(edge)]
        return T.lamination(intersections)

    return curve_from_pattern

def make_pencil(edge_list,
                pattern_converter,
                raw_triangles,
                patterns,*args):
    '''Given raw data for a pencil, returns the relevant curver objects.

    INPUT:

    edge_list

        a list of edge labels

    patt_conv

        a function for colouring edges according to a pattern

    raw_triangles

        a list of triples of oriented edges

    patterns

        a list of patterns of geometric intersection numbers

    OUTPUT:

    T

        A punctured surface with an ideal triangulation.

    curves

        A list of simple closed curves on the surface.
    
    curve_from_hitlist

        Function, which, given a list of edges hit by a curve,
        returns the corresponding curve (as a curver.Lamination).
    
    multi_curves

        A list of multicurves (one for each pattern) of which the
        curves are components. Sometimes it's useful to know which
        curves came from which multicurve.

    '''
    edge_converter=converter(edge_list)
    triangle_converter=lambda tri:(
        list(map(edge_converter,tri)))
    triangles=[triangle_converter(tri)
               for tri in raw_triangles]
    T=curver.create_triangulation(triangles)
    curve_from_pattern=get_curve_from_pattern(T,edge_list,
                                              pattern_converter)
    multi_curves=[curve_from_pattern(patt)
                  for patt in patterns]
    curves=[curve
            for multi_curve in multi_curves
            for curve in list(multi_curve.components().keys())]
    curve_from_hitlist=get_curve_from_hitlist(T,edge_list)
    return T,curves,curve_from_hitlist,multi_curves

