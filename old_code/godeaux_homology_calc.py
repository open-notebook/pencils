#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from gauss import gauss
import curver

T,curves,curve_from_hitlist,multi_curves=make_pencil(*z5_godeaux())
hitlists=[[(0,1),(0,2),(0,3),(0,4)],
          [(1,1),(1,2),(1,3),(1,4)],
          [(3,1),(3,2),(3,3),(3,4)],
          [(4,1),(4,2),(4,3),(4,4)],
          [(1,0),(1,4),(4,5),(4,1),(4,4),(2,5),(2,1),(2,4),(0,5),(0,2)],
          [(2,0),(2,1),(2,5),(4,4),(4,1),(4,5),(1,3)],
          [(3,0),(2,3),(0,5),(0,2),(0,3),(3,5),(3,1)],                    
          [(4,0),(4,4),(2,5),(2,2),(2,3),(0,5),(0,2),(0,3),(3,5),(3,2)]]

homology_basis=[curve_from_hitlist(hitlist)
                for hitlist in hitlists]

def homology_class(input_curve,basis):
    # each curve gives us a vector of integers mod 2 by taking its
    # Z/2-intersection number with all the basis curves
    def vect_to_list(curve):
        return [curve.intersection(b) for b in basis]
    A=[vect_to_list(b) for b in basis]
    RHS=vect_to_list(input_curve)
    gauss_prob=[A[i]+[RHS[i]]
                for i in range(0,len(A))]
    return gauss(gauss_prob)

def find_monodromy(monodromies,curve,basis):
    cl_c=homology_class(curve,basis)
    return (sum(monodromies[i]*cl_c[i]
                for i in range(0,len(basis)))) % 2

def mon_from_quad(x,y,z,w):
    return [x,y,y,x,z,w,w,z]
    
mon_list=[mon_from_quad(X,Y,Z,W)
          for X in [0,1]
          for Y in [0,1]
          for Z in [0,1]
          for W in [0,1]]

#print(check_relation(T,curves))
#for mon in mon_list:
#    curve_mon=[find_monodromy(mon,curve,homology_basis) for curve in curves]
#if sum(curve_mon)==8:
#    print(mon,curve_mon,sum(curve_mon))

#curver.show([curves[2],curves[3],curves[4],curves[5],curves[6]]+homology_basis)

def cl_from_intersections(lst,basis):
    def vect_to_list(curve):
        return [curve.intersection(b) for b in basis]
    A=[vect_to_list(b) for b in basis]
    gauss_prob=[A[i]+[lst[i]]
                for i in range(0,len(A))]
    return gauss(gauss_prob)

def cl_of_branch_cut(mon):
    return cl_from_intersections(mon,homology_basis)

geom_int_for_basis={i:homology_basis[i].geometric for i in range(0,8)}
for mon in mon_list:
    cu=cl_of_branch_cut(mon)
    branch_cut=[0 for x in range(0,30)]
    for u in range(0,8):
        if cu[u]==1:
            branch_cut=[(branch_cut[i]+geom_int_for_basis[u][i])%2
                        for i in range(0,30)]
    print(branch_cut)
#for mon in mon_list:
#    print('\n',mon)
#    print('[',end='')
#    for i in range(0,30):
#        print(sum(geom_int_for_basis[j][i]*cl_of_branch_cut(mon)[j]
#                  for j in range(0,8)),end=',')
#    print(']')
