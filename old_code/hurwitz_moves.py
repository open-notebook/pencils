#!/usr/bin/python3

import curver
from curver_aux import *
from monodromy_data import *

# Input should be:
# - a triangulation
# - an ordered list of multicurves
# - an ordered list of labels
# - a choice of Hurwitz move
#
# To encode a choice of Hurwitz move we need to pick two (cyclically)
# consecutive indices in the list of multicurves such that the
# corresponding vanishing paths will cross and then specify which
# critical point goes "under" (i.e. stays closer to the basepoint and
# therefore remains unchanged). We can do this by writing the
# unchanged index first, so (1,2) means that critical point 1 goes
# under critical point 2 while (2,1) means that critical point 2 goes
# under critical point 1.


def order_clockwise(i,j,n):
    '''Given two neighbouring points in a clockwise cycle of length n,
    returns the same two points ordered clockwise.

    For example,
    order_clockwise(1,2,12) returns 1,2
    order_clockwise(1,12,12) returns 12,1

    '''
    if n==2 and i!=j:
        return min(i,j),max(i,j)
    if (i-j)%n==1:
        return j,i
    elif (j-i)%n==1:
        return i,j
    else:
        raise Exception('Arguments are not cyclically consecutive')

def hurwitz(T,multi_curves,multi_labels,move_labels):
    '''Performs a Hurwitz move on a pencil.
    
    The data of a pencil comprises a (cyclically) ordered list of
    multi-curves on a surface. You should think of the following
    picture: you have a fibration over the plane (obtained by excising
    the fibre over infinity in our pencil); the fibre over the origin
    we will call T. The critical values are connected to the origin by
    straight line paths ("vanishing paths"). Over each vanishing path,
    you get a multi-curve ("vanishing cycle") in T, which collapses to
    the critical point as you move along the vanishing path.

    The global monodromy of a pencil is the product of Dehn twists in
    the vanishing cycles; if the critical values are (ordered
    clockwise) [v_1,...,v_n] then the global monodromy is
    \tau_{v_1}...\tau_{v_n}.
    
    Now you can start moving the critical values around.

    - Move (w,v): If w and v are cyclically ordered clockwise and w
      passes under v (closer to the origin) then you should replace
      the vanishing cycle for w by \tau_w(v) and the vanishing cycle
      for v by w.

    - Move (v,w): If v passes under w then you should replace the
      vanishing cycle w by v and v by \tau_v^{-1}(w).

    These two moves are called Hurwitz moves, and preserve the global monodromy:

    \tau_{\tau_w(v)}\tau_w = \tau_w\tau_v\tau_w^{-1} \tau_w = \tau_w\tau_v

    \tau_v\tau_{\tau_v^{-1}(w)} = \tau_v \tau_v^{-1}\tau_w\tau_v = \tau_w\tau_v

    Most of the time, we need to handle (...,w,v,...) but the cyclic
    order poses an awkward edge-case (v,...,w).

    '''

    # Our whole list of multicurves is ordered cyclically clockwise to
    # begin with. Let w,v be the curves we wish to swap, given in that
    # order.
    #
    # Cases to handle:
    # Case 1: (v,a,......,b,w)
    # Case 2: (a,...,w,v,...b)
    #
    # Move (w,v) means w goes under v (i.e. (w',v') = (T_w(v),w))
    # while (v,w) means v goes under w (i.e. (w',v') = (v,T_v^{-1}(w)))
    # the end result should be
    # (w',a,......,b,v')
    # (a,...,w',v',...b)
    #
    # The point of multi_labels/move_labels: I want to label my
    # multicurves 0,1,2,3,4,5,... to start with and keep calling them
    # the same thing as I track through a sequence of Hurwitz moves.
    # As I apply moves, these labels will no longer refer to the index
    # of the multicurve in the list of multicurves, so the dictionary
    # multi_labels has the property that multi_labels[i] tells you the
    # index of the multicurve formerly known as m in the new list
    n=len(multi_curves)
    # Determine whether we're doing move (w,v) or (v,w)
    move=tuple(multi_labels[m] for m in move_labels)
    i,j=order_clockwise(*move,n)
    w,v=multi_curves[i],multi_curves[j]
    if (i,j)==move:
        w_new,v_new=(multi_twist(w))(v),w
    else:
        w_new,v_new=v,(multi_twist(v).inverse())(w)

    if i==n-1:
        new_list=[v_new]+multi_curves[1:n-1]+[w_new]
    elif j==n-1:
        new_list=multi_curves[0:i]+[w_new,v_new]
    else:
        new_list=multi_curves[0:i]+[w_new,v_new]+multi_curves[j+1:]
    new_multi_labels={m : multi_labels[m]
                      for m in multi_labels.keys()
                      if m not in move_labels}
    new_multi_labels.update({move_labels[0] : multi_labels[move_labels[1]],
                             move_labels[1] : multi_labels[move_labels[0]]})
    return T,new_list,new_multi_labels

def hurwitz_moves(T,init_curves,init_labels,move_list):
    if move_list:
        next_move=move_list.pop()
        T,next_curves,next_labels=hurwitz(T,init_curves,init_labels,next_move)
        return hurwitz_moves(T,next_curves,next_labels,move_list)
    else:
        return T,init_curves,init_labels

