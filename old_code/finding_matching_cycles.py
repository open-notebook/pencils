#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from double_cover import *
from itertools import combinations
from hurwitz_moves import *

def union(T,a,b):
    if a==b:
        gi_list=a.geometric
    else:
        gi_list=[x[0]+x[1]
                 for x in zip(a.geometric,b.geometric)]
    return T.lamination(gi_list)


def colour_quintuple(T,multicurve,inv):
    cpnts=list(multicurve.components().keys())
    list_of_unions=[union(T,cpt,inv(cpt))
                    for cpt in cpnts]
    list_of_cpt_counts=[len(uni.components().keys())
                        for uni in list_of_unions]
    singleton_index=list_of_cpt_counts.index(1)
    list_of_duplets=list({uni for uni in list_of_unions
                          if uni!=cpnts[singleton_index]})
    return [cpnts[singleton_index]]+list_of_duplets

g_edges,g_patt_conv,g_raw_tri,g_patts=z5_godeaux()
G,g_curves,g_from_hitlist,g_multi=make_pencil(g_edges,g_patt_conv,g_raw_tri,g_patts)
#
# Define the involution \sigma:
#
edge_conv=converter(g_edges)
edge_dict={(0,(0,0)) : (1,(0,0)),# fixed edges
           (0,(1,5)) : (1,(1,5))}
part1={(0,(0,1)) : (0,(4,3)),# zeroth square, fourth square
       (0,(0,2)) : (0,(4,4)),
       (0,(0,3)) : (0,(4,1)),
       (0,(0,4)) : (0,(4,2)),
       (0,(1,0)) : (1,(4,0)),# first square, third square
       (0,(1,1)) : (0,(3,3)),
       (0,(1,2)) : (0,(3,4)),
       (0,(1,3)) : (0,(3,1)),
       (0,(1,4)) : (0,(3,2)),
       (0,(2,0)) : (1,(3,0)),# second square
       (0,(2,1)) : (0,(2,3)),
       (0,(2,2)) : (0,(2,4)),
       (0,(4,5)) : (1,(3,5)),
       (0,(2,5)) : (1,(0,5))}
part2={part1[a] : a for a in part1} # involution!
edge_dict.update(part1)
edge_dict.update(part2)
label_dict={edge_conv(a) : edge_conv(edge_dict[a])
            for a in edge_dict}
sigma=G.find_isometry(G,label_dict)
# First need to split up our multicurves into submulticurves and
# figure out which ones correspond to the diagram coming out of the
# numerical simulation. For now, let's call them V[1],...,V[15] with
# V[0] and V[16] being the vanishing cycles over 0 and infinity. Let's
# call the corresponding twists T[i]. The relation we want to test is
# the following.
#
# define V
triplets=[colour_quintuple(G,g_multi[i],sigma)
          for i in range(2,7)]
move_lst=((4,15),(4,11),(4,9),(4,0),(4,8),(4,3),(4,6),(4,5),(4,7),(7,5),# green moves
          (6,7),(3,7),(3,6),(3,5),(3,4),(15,10),(15,12),(2,13),(13,1),(13,'infty'), # red moves
          (14,15),(13,15),(13,14),(10,11),(0,9),(0,10),(0,11),(0,12),(0,13),(0,14),(0,15))# initial moves
#initial_multi_curves=[g_multi[0],g_multi[1]]+[multi for triplet in triplets
#                                              for multi in triplet]
# get the order right to begin with
initial_multi_curves=[g_multi[0],g_multi[1],
                      triplets[0][0],triplets[0][1],triplets[0][2],
                      triplets[1][1],triplets[1][2],triplets[1][0],
                      triplets[2][1],triplets[2][0],triplets[2][2],
                      triplets[3][1],triplets[3][2],triplets[3][0],
                      triplets[4][1],triplets[4][2],triplets[4][0]]
initial_multi_labels={
    0 : 0,
    'infty' : 1,
    1 : 2, # one crit pt
    2 : 3,
    3 : 4,
    4 : 5,
    5 : 6,
    6 : 7, # one crit pt
    7 : 8,
    8 : 9, # one crit pt
    9 : 10,
    10 : 11,
    11 : 12,
    12 : 13, # one crit pt
    13 : 14,
    14 : 15,
    15 : 16, # one crit pt
}
for a in range(0,2):
    initial_multi_curves[3],initial_multi_curves[4]=initial_multi_curves[3+a],initial_multi_curves[4-a]
    for b in range(0,2):
        initial_multi_curves[5],initial_multi_curves[6]=initial_multi_curves[5+b],initial_multi_curves[6-b]
        for c in range(0,2):
            initial_multi_curves[8],initial_multi_curves[10]=initial_multi_curves[8+2*c],initial_multi_curves[10-2*c]
            for d in range(0,2):
                initial_multi_curves[11],initial_multi_curves[12]=initial_multi_curves[11+d],initial_multi_curves[12-d]
                for e in range(0,2):
                    initial_multi_curves[14],initial_multi_curves[15]=initial_multi_curves[14+e],initial_multi_curves[15-e]
                    after_hurwitz=hurwitz_moves(G,initial_multi_curves,initial_multi_labels,list(move_lst))
                    after_curves=after_hurwitz[1]
                    after_labels=after_hurwitz[2]
                    if after_curves[after_labels[4]]==after_curves[after_labels[10]] and after_curves[after_labels[7]]==after_curves[after_labels[13]]:
                        print("yes")
                    else:
                        print("no")

                    
# the output of this reveals that a,b,c,d,e=0,0,1,1,1 is the only choice which works
