#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from double_cover import *
from itertools import combinations
from hurwitz_moves import *

def union(T,a,b):
    if a==b:
        gi_list=a.geometric
    else:
        gi_list=[x[0]+x[1]
                 for x in zip(a.geometric,b.geometric)]
    return T.lamination(gi_list)


def colour_quintuple(T,multicurve,inv):
    cpnts=list(multicurve.components().keys())
    list_of_unions=[union(T,cpt,inv(cpt))
                    for cpt in cpnts]
    list_of_cpt_counts=[len(uni.components().keys())
                        for uni in list_of_unions]
    singleton_index=list_of_cpt_counts.index(1)
    list_of_duplets=list({uni for uni in list_of_unions
                          if uni!=cpnts[singleton_index]})
    return [cpnts[singleton_index]]+list_of_duplets

def hurwitzed():
    g_edges,g_patt_conv,g_raw_tri,g_patts=z5_godeaux()
    G,g_curves,g_from_hitlist,g_multi=make_pencil(g_edges,g_patt_conv,g_raw_tri,g_patts)
    #
    # Define the involution \sigma:
    #
    edge_conv=converter(g_edges)
    edge_dict={(0,(0,0)) : (1,(0,0)),# fixed edges
               (0,(1,5)) : (1,(1,5))}
    part1={(0,(0,1)) : (0,(4,3)),# zeroth square, fourth square
           (0,(0,2)) : (0,(4,4)),
           (0,(0,3)) : (0,(4,1)),
           (0,(0,4)) : (0,(4,2)),
           (0,(1,0)) : (1,(4,0)),# first square, third square
           (0,(1,1)) : (0,(3,3)),
           (0,(1,2)) : (0,(3,4)),
           (0,(1,3)) : (0,(3,1)),
           (0,(1,4)) : (0,(3,2)),
           (0,(2,0)) : (1,(3,0)),# second square
           (0,(2,1)) : (0,(2,3)),
           (0,(2,2)) : (0,(2,4)),
           (0,(4,5)) : (1,(3,5)),
           (0,(2,5)) : (1,(0,5))}
    part2={part1[a] : a for a in part1} # involution!
    edge_dict.update(part1)
    edge_dict.update(part2)
    label_dict={edge_conv(a) : edge_conv(edge_dict[a])
                for a in edge_dict}
    sigma=G.find_isometry(G,label_dict)
    triplets=[colour_quintuple(G,g_multi[i],sigma)
              for i in range(2,7)]
    initial_multi_curves=[g_multi[0],g_multi[1],
                          triplets[0][0],triplets[0][1],triplets[0][2],
                          triplets[1][1],triplets[1][2],triplets[1][0],
                          triplets[2][2],triplets[2][0],triplets[2][1],
                          triplets[3][1],triplets[3][2],triplets[3][0],
                          triplets[4][1],triplets[4][2],triplets[4][0]]
    initial_multi_labels={
        0 : 0,
        'infty' : 1,
        1 : 2, # singlet
        2 : 3, # pair over 1
        3 : 4, # pair over 1
        4 : 5, # pair over e^{-2\pi i/5}
        5 : 6, # pair over e^{-2\pi i/5}
        6 : 7, # singlet
        7 : 8, # pair over e^{-4\pi i/5}
        8 : 9, # singlet
        9 : 10, # pair over e^{-4\pi i/5}
        10 : 11, # pair over e^{-6\pi i/5}
        11 : 12, # pair over e^{-6\pi i/5}
        12 : 13, # singlet
        13 : 14, # pair over e^{-8\pi i/5}
        14 : 15, # pair over e^{-8\pi i/5}
        15 : 16, # singlet
    }
    move_lst=((4,15),(4,11),(4,9),(4,0),(4,8),(4,3),(4,6),(4,5),(4,7),(7,5),# green moves
              (6,7),(3,7),(3,6),(3,5),(3,4),(15,10),(15,12),(2,13),(13,1),(13,'infty'), # red moves
              (14,15),(13,15),(13,14),(10,11),(0,9),(0,10),(0,11),(0,12),(0,13),(0,14),(0,15))# initial moves
    after_hurwitz=hurwitz_moves(G,initial_multi_curves,initial_multi_labels,list(move_lst))
    after_curves=after_hurwitz[1]
    after_labels=after_hurwitz[2]
    #print("Curves ",after_labels[4]," and ",after_labels[10]," form a matching pair")
    #assert(after_curves[after_labels[4]]==after_curves[after_labels[10]])
    #print("Curves ",after_labels[7]," and ",after_labels[13]," form a matching pair")
    #assert(after_curves[after_labels[7]]==after_curves[after_labels[13]])
    edge_list=[e.label for e in G.positive_edges]
    pattern_converter=lambda x: x
    raw_triangles=[[conv_label(e,len(edge_list))
                    for e in tri]
                   for tri in G.triangles]
    patterns=[curve.geometric for curve in after_curves]
    return edge_list,pattern_converter,\
        raw_triangles,patterns


        
                    
