#!/usr/bin/python3

import curver
from curver_aux import *
from monodromy_data import *
from double_cover import *
from itertools import combinations
from hurwitz_moves import *
from z5_godeaux_after_hurwitz import *
from barlow_double_cover_data import *

Z,curves,tau=barlow_double_cover_ready_made()
new_edges=[]
tau_dict=tau.label_map
for edge in Z.indices:
    if tau_dict[edge] not in new_edges and ~tau_dict[edge] not in new_edges:
        new_edges.append(edge)

def strip_edge(edge):
    if str(edge)[0]=='~':
        return 1,int(str(edge)[1::])
    else:
        return 0,edge
    
def edge_conv_fn(edge):
    orient,edge_lab=strip_edge(edge)
    torient,tedge_lab=strip_edge(tau_dict[edge])
    if edge_lab in new_edges:
        return (orient,edge_lab)
    if tedge_lab in new_edges:
        return (torient,tedge_lab)
    raise Exception("Edge not accounted for")

new_tris=[[edge_conv_fn(edge) for edge in tri] for tri in Z.triangles]

raw_triangles=[]
for tri in new_tris:
    print(tri)
    tri1=[x for x in raw_triangles if x==tri]
    tri2=[x for x in raw_triangles if x==[tri[1],tri[2],tri[0]]]
    tri3=[x for x in raw_triangles if x==[tri[2],tri[0],tri[1]]]
    if not tri1 and not tri2 and not tri3:
        raw_triangles.append(tri)

def get_curve(c):
    if tau(c)==c:
        return 'doubled',[c.geometric[i] for i in range(0,60) if i in new_edges]
    else:
        lst1=[c.geometric[i] for i in range(0,60)]
        lst2=[tau(c).geometric[i] for i in range(0,60)]
        lst3=[lst1[i]+lst2[i] for i in range(0,60)]
        return 'nondoubled',[lst3[i] for i in range(0,60) if i in new_edges]
        
downstairs_curves=[]
for c in curves:
    doubled,cgi=get_curve(c)
    if cgi not in downstairs_curves:
        if doubled=='doubled':
            downstairs_curves.append(cgi)
            downstairs_curves.append(cgi)
        else:
            downstairs_curves.append(cgi)
            
for crv in downstairs_curves:
    print(crv)
print(len(downstairs_curves))
barlow,emptycurves,emptyhitlist,emptymulti=make_pencil(new_edges,lambda x:x,raw_triangles,[])
barlow_curves=[barlow.lamination(c) for c in downstairs_curves]
print(check_relation(barlow,barlow_curves))
for c in barlow_curves:
    print(c.is_separating())
print(barlow.triangles)
    
'''
To save you running the program, here is the output.

Here is the triangulation data for the fibre

[(~29, ~28, 27), (~27, ~8, ~25), (~26, ~23, 22), (~24, ~9, ~12), (~22, ~18, ~19), (~21, 29, 28), (~20, ~17, ~0), (~16, 10, 9), (~15, 4, 19), (~14, ~13, 12), (~11, 8, 23), (~10, ~7, 6), (~6, ~2, ~3), (~5, 14, 13), (~4, ~1, 0), (1, 5, 2), (3, 16, 20), (7, 11, 24), (15, 26, 25), (17, 21, 18)]


Here is the list barlow_curves

[1, 0, 1, 0, 1, 1, 1, 2, 1, 0, 1, 3, 1, 2, 1, 1, 1, 0, 1, 0, 1, 1, 1, 2, 1, 0, 1, 1, 2, 1]
[0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 2, 1, 2, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 2, 1]
[0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0]
[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0]
[0, 0, 1, 0, 0, 1, 1, 3, 1, 2, 2, 2, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 3, 1, 2, 2, 1, 1, 0]
[1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0, 0, 1, 0, 1, 1, 1, 2, 0, 2, 1, 1, 1, 0]
[1, 0, 1, 0, 1, 1, 1, 2, 0, 2, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 2, 1]
[0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0]
[0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0]
[0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0]
[1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0]
[1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0]
[1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0]
[1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0]
[1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0]
[1, 1, 1, 2, 0, 2, 3, 3, 0, 2, 2, 3, 2, 2, 0, 1, 2, 1, 1, 1, 0, 2, 2, 3, 0, 0, 1, 0, 1, 1]
[1, 1, 1, 1, 0, 2, 2, 3, 0, 0, 1, 3, 0, 1, 1, 2, 1, 1, 1, 2, 0, 2, 3, 3, 0, 2, 2, 2, 2, 0]
[1, 1, 1, 0, 0, 2, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 2, 2, 2, 0, 1, 2, 2, 2, 0]
[1, 1, 1, 1, 0, 2, 2, 2, 0, 1, 2, 1, 2, 2, 0, 0, 1, 1, 1, 0, 0, 2, 1, 1, 1, 0, 0, 0, 1, 1]
[0, 0, 1, 1, 0, 1, 2, 2, 0, 1, 2, 2, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 2, 2, 0, 1, 2, 1, 1, 0]
[0, 0, 1, 0, 0, 1, 1, 2, 0, 1, 1, 2, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 2, 0, 1, 1, 1, 1, 0]
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0]
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0]
[1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1]

'''

