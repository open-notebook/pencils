#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from double_cover import *
from z5_godeaux_after_hurwitz import *
from gauss import *

def barlow_double_cover():
    # Start by setting up the Z/5-Godeaux and performing Hurwitz moves
    # to get the matching cycles for the nodal degeneration into
    # position
    T,curves,curve_from_hitlist,multi_curves=make_pencil(*hurwitzed())
    # Get a basis of closed curves for the Z/2-homology of the fibre
    # of the bicanonical pencil on the Z/5-Godeaux
    n=5
    edge_list=[(j,k)
               for j in range(0,n)
               for k in range(0,6)]
    hitlist_converter=lambda x: [edge_list.index(y) for y in x]
    hitlists=[hitlist_converter(x)
              for x in [[(0,1),(0,2),(0,3),(0,4)],
                        [(1,1),(1,2),(1,3),(1,4)],
                        [(3,1),(3,2),(3,3),(3,4)],
                        [(4,1),(4,2),(4,3),(4,4)],
                        [(1,0),(1,4),(4,5),(4,1),(4,4),(2,5),(2,1),(2,4),(0,5),(0,2)],
                        [(2,0),(2,1),(2,5),(4,4),(4,1),(4,5),(1,3)],
                        [(3,0),(2,3),(0,5),(0,2),(0,3),(3,5),(3,1)],                    
                        [(4,0),(4,4),(2,5),(2,2),(2,3),(0,5),(0,2),(0,3),(3,5),(3,2)]]]
    homology_basis=[curve_from_hitlist(hitlist)
                    for hitlist in hitlists]

    def find_monodromy(monodromies,curve,basis):
        cl_c=homology_class(curve,basis)
        return (sum(monodromies[i]*cl_c[i]
                    for i in range(0,len(basis)))) % 2
    
    def mon_from_quad(x,y,z,w):
        return [x,y,y,x,z,w,w,z]
    
    mon_list=[mon_from_quad(X,Y,Z,W)
              for X in [0,1]
              for Y in [0,1]
              for Z in [0,1]
              for W in [0,1]]

    # Recall that the multicurves which give matching cycles are
    # multi_curves[3]=multi_curves[4] and
    # multi_curves[13]=multi_curves[14]. After passing to component
    # curves, this becomes curves[4,5,6,7,20,21,22,23].
    assert multi_curves[3]==multi_curves[4]
    assert multi_curves[13]==multi_curves[14]
    for i in range(0,27):
        if i in [4,5,6,7]:
            assert curves[i] in multi_curves[3].components()
        elif i in [20,21,22,23]:
            assert curves[i] in multi_curves[13].components()
        else:
            assert curves[i] not in multi_curves[3].components()
            assert curves[i] not in multi_curves[13].components()


    def find_monodromy(monodromies,curve,basis):
        '''Given the monodromy of a Z/2-cover around a Z/2-basis, finds the
        monodromy around a curve.'''
        cl_c=homology_class(curve,basis)
        return (sum(monodromies[i]*cl_c[i]
                    for i in range(0,len(basis)))) % 2

    def monodromy_from_quadruple(x,y,z,w):
        '''Generates the monodromy of a \sigma-invariant cover'''
        return [x,y,y,x,z,w,w,z]

    # All possible monodromies of Z/2-covers to which \sigma lifts
    mon_list=[monodromy_from_quadruple(X,Y,Z,W)
              for X in [0,1]
              for Y in [0,1]
              for Z in [0,1]
              for W in [0,1]]

    # We are looking for choices of monodromy such that the curves in
    # our set of vanishing cycles with nontrivial monodromy are
    # precisely curves 4,5,6,7,20,21,22,23. It turns out that the
    # unique choice is [1,0,0,1,0,0,0,0].
    for mon in mon_list:
        curve_mon=[find_monodromy(mon,curve,homology_basis) for curve in curves]
        nontrivial_indices=[i for i in range(0,len(curve_mon))
                            if curve_mon[i]!=0]
        if mon==[1,0,0,1,0,0,0,0]:
            assert nontrivial_indices==[4,5,6,7,20,21,22,23]
        else:
            assert nontrivial_indices!=[4,5,6,7,20,21,22,23]
    # Given this choice of monodromy, we need to find a branch cut
    # which intersects each edge (mod 2) the same number of times as
    # every element of our homology basis on which the monodromy is
    # nontrivial
    mon=monodromy_from_quadruple(1,0,0,0)
    homology_class_of_branch_cut=dual_curve_to_homology_class(mon,homology_basis)
    geom_int_for_basis={i:homology_basis[i].geometric for i in range(0,8)}
    branch_cut=[0 for x in range(0,30)]
    for u in range(0,8):
        if homology_class_of_branch_cut[u]==1:
            branch_cut=[(branch_cut[i]+geom_int_for_basis[u][i])%2
                        for i in range(0,30)]
    assert branch_cut==[0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1,
                        0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0]
    # Finally, we need to take the corresponding double cover of the
    # surface which is the fibre of the bicanonical on Z/5-Godeaux,
    # drop four of the vanishing cycles and check that the result is
    # still a relation in the mapping class group. When we pass to the
    # double cover, the curves other than curves[4,5,6,7,20,21,22,23]
    # lift to pairs of curves in the cover. The indexing therefore
    # means that curves[4,5,6,7,20,21,22,23] lift to curves
    # 8,9,10,11,36,37,38,39 in the cover. So we need to drop one from
    # each of the following pairs: {8,10}, {9,11}, {36,38},
    # {37,39}. Any choice will do.
    Z,z_curves,z_from_histlit,z_multi=make_pencil(*double_cover(branch_cut,
                                                                T,curves))
    lift_cpnts=[len(multicurve.components())
                for multicurve in z_multi]
    connected_lifts=[x for x in range(0,len(lift_cpnts))
                     if lift_cpnts[x]==1]
    assert connected_lifts==[4,5,6,7,20,21,22,23]
    droppings=[8,9,36,37]
    new_curves=[z_curves[x]
                for x in range(0,len(z_curves))
                if x not in droppings]
    assert check_relation(Z,new_curves)
    edge_list=Z.positive_edges
    pattern_converter=lambda x: x
    raw_triangles=[[conv_label(e,len(edge_list))
                    for e in tri] for tri in Z.triangles]
    patterns=[curve.geometric for curve in new_curves]
    return edge_list,pattern_converter,\
        raw_triangles,patterns
