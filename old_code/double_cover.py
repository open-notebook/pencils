'''Module for taking double covers

The most important function is double_cover, which returns the data
which can be fed to curver_aux.make_pencil() to generate the curver
objects corresponding to the double cover. For example,

make_pencil(*double_cover(branch_cut,*z5_godeaux()))

will give the double cover of the fibre of the bicanonical pencil on
the Z/5-Godeaux surface if you specify a suitable branch cut.

'''

def conv_label(oriented_edge,number_of_edges):
    '''Converts back from curver notation for edge orientations to mine.'''
    edge_string=str(oriented_edge)
    if edge_string[0]=='~':
        return (1,int(edge_string[1::]))
    else:
        return (0,int(edge_string))

def change_sheet(sheet):
    '''Changes sheets in the cover'''
    change={'A':'B','B':'A'}
    return change[sheet]

def process_edges(i,tri,number_of_edges,sheet,branch_cut):
    '''A recursive procedure which returns a triangle in the double cover.

    If an edge downstairs is labelled by a then upstairs there are two
    edges, (a,'A') and (a,'B') living over it (by path-lifting). We
    choose to label edges in such a way that edge (a,'A') starts on
    sheet 'A' (remember that our edges are oriented).

    To lift a triangle, you need to know which sheet you're going to
    start on; then you work around the triangle, in the direction of
    its orientation, and figure out how the sheet changes. For
    example, suppose you traverse edge a downstairs and edge a has odd
    intersection with the branch curve. What happens?

    - you switch sheet.

    - If you start on sheet 'A' and you're traversing edge a backwards
      then the edge you're traversing upstairs is (a,'B') (while it
      would be (a,'A') if you were traversing it forwards)

    These considerations allow you to figure out which lifted edges
    you're traversing as you go around the lifted triangle.

    '''
    orientation,edge=conv_label(tri[i],number_of_edges)
    if (branch_cut[edge])%2==0:
        new_sheet=sheet
        edge_label=(orientation,(edge,sheet))
    else:
        new_sheet=change_sheet(sheet)
        if orientation==0:
            edge_label=(orientation,(edge,sheet))
        else:
            edge_label=(orientation,(edge,new_sheet))
    if i==len(tri)-1:
        return [edge_label]
    else:
        return [edge_label]+process_edges(i+1,
                                          tri,
                                          number_of_edges,
                                          new_sheet,
                                          branch_cut)


def double_cover(branch_cut,T,curves,*args):
    '''Takes a triangulated surface T with a collection C of curves and a
    branch cut, and returns the surface which double covers T as well as
    the collection of multicurves which cover C.

    In other words, you take two copies of T cut open along the branch
    curve and you glue them along their common boundary.

    '''
    edges=T.positive_edges
    number_of_edges=len(edges)
    triangles=T.triangles
    edge_list=[(e.label,s)
               for s in {'A','B'}
               for e in edges]
    pattern_converter=lambda duple: duple[0]
    raw_triangles=[process_edges(0,tri,number_of_edges,s,branch_cut)
                   for s in {'A','B'}
                   for tri in triangles]
    patterns=[curve.geometric for curve in curves]
    return edge_list,pattern_converter,\
        raw_triangles,patterns

def godeaux_double_cover():
    '''An old version where I did it by hand. Keeping it until I've tested
    the automated version.'''
    
    edge_list=[(i,j,k)
               for i in range(0,5)
               for j in range(0,6)
               for k in {'a','b'}]
    edges_downstairs=[(i,j)
                      for i in range(0,5)
                      for j in range(0,6)]
    #pattern_converter=lambda tripl: tripl[1]
    pattern_converter=lambda tripl: edges_downstairs.index(tripl[:-1])
    
    Aa_tri=[[(1, (0, 4, 'a')), (1, (0, 1, 'a')), (0, (0, 0, 'a'))],
            [(1, (1, 4, 'a')), (1, (1, 1, 'a')), (0, (1, 0, 'a'))],
            [(1, (2, 4, 'a')), (1, (2, 1, 'a')), (0, (2, 0, 'a'))],
            [(1, (3, 4, 'a')), (1, (3, 1, 'b')), (0, (3, 0, 'b'))],
            [(1, (4, 4, 'a')), (1, (4, 1, 'a')), (0, (4, 0, 'a'))]]
    
    Ba_tri=[[(1, (1, 0, 'a')), (1, (0, 2, 'a')), (1, (0, 3, 'a'))],
            [(1, (2, 0, 'a')), (1, (1, 2, 'b')), (1, (1, 3, 'a'))],
            [(1, (3, 0, 'a')), (1, (2, 2, 'a')), (1, (2, 3, 'a'))],
            [(1, (4, 0, 'a')), (1, (3, 2, 'a')), (1, (3, 3, 'a'))],
            [(1, (0, 0, 'a')), (1, (4, 2, 'b')), (1, (4, 3, 'b'))]]
    
    Ca_tri=[[(0, (0, 5, 'a')), (0, (0, 2, 'a')), (0, (0, 1, 'a'))],
            [(0, (1, 5, 'a')), (0, (1, 2, 'b')), (0, (1, 1, 'a'))],
            [(0, (2, 5, 'a')), (0, (2, 2, 'a')), (0, (2, 1, 'a'))],
            [(0, (3, 5, 'a')), (0, (3, 2, 'b')), (0, (3, 1, 'b'))],
            [(0, (4, 5, 'a')), (0, (4, 2, 'b')), (0, (4, 1, 'a'))]]
    
    Da_tri=[[(0, (0, 4, 'a')), (0, (0, 3, 'b')), (1, (3, 5, 'a'))],
            [(0, (1, 4, 'a')), (0, (1, 3, 'a')), (1, (4, 5, 'a'))],
            [(0, (2, 4, 'a')), (0, (2, 3, 'a')), (1, (0, 5, 'a'))],
            [(0, (3, 4, 'a')), (0, (3, 3, 'b')), (1, (1, 5, 'a'))],
            [(0, (4, 4, 'a')), (0, (4, 3, 'a')), (1, (2, 5, 'a'))]]
    
    Ab_tri=[[(1, (0, 4, 'b')), (1, (0, 1, 'b')), (0, (0, 0, 'b'))],
            [(1, (1, 4, 'b')), (1, (1, 1, 'b')), (0, (1, 0, 'b'))],
            [(1, (2, 4, 'b')), (1, (2, 1, 'b')), (0, (2, 0, 'b'))],
            [(1, (3, 4, 'b')), (1, (3, 1, 'a')), (0, (3, 0, 'a'))],
            [(1, (4, 4, 'b')), (1, (4, 1, 'b')), (0, (4, 0, 'b'))]]
    
    Bb_tri=[[(1, (1, 0, 'b')), (1, (0, 2, 'b')), (1, (0, 3, 'b'))],
            [(1, (2, 0, 'b')), (1, (1, 2, 'a')), (1, (1, 3, 'b'))],
            [(1, (3, 0, 'b')), (1, (2, 2, 'b')), (1, (2, 3, 'b'))],
            [(1, (4, 0, 'b')), (1, (3, 2, 'b')), (1, (3, 3, 'b'))],
            [(1, (0, 0, 'b')), (1, (4, 2, 'a')), (1, (4, 3, 'a'))]]
    
    Cb_tri=[[(0, (0, 5, 'b')), (0, (0, 2, 'b')), (0, (0, 1, 'b'))],
            [(0, (1, 5, 'b')), (0, (1, 2, 'a')), (0, (1, 1, 'b'))],
            [(0, (2, 5, 'b')), (0, (2, 2, 'b')), (0, (2, 1, 'b'))],
            [(0, (3, 5, 'b')), (0, (3, 2, 'a')), (0, (3, 1, 'a'))],
            [(0, (4, 5, 'b')), (0, (4, 2, 'a')), (0, (4, 1, 'b'))]]
    
    Db_tri=[[(0, (0, 4, 'b')), (0, (0, 3, 'a')), (1, (3, 5, 'b'))],
            [(0, (1, 4, 'b')), (0, (1, 3, 'b')), (1, (4, 5, 'b'))],
            [(0, (2, 4, 'b')), (0, (2, 3, 'b')), (1, (0, 5, 'b'))],
            [(0, (3, 4, 'b')), (0, (3, 3, 'a')), (1, (1, 5, 'b'))],
            [(0, (4, 4, 'b')), (0, (4, 3, 'b')), (1, (2, 5, 'b'))]]
    raw_triangles=Aa_tri+Ba_tri+Ca_tri+Da_tri+\
                   Ab_tri+Bb_tri+Cb_tri+Db_tri
    
    #patterns=[[1,1,0,1,0,1],[1,0,1,0,1,1]]
    #patterns.extend([[2*p,2*p+1,1,2*p+1,1,2*p]
    #                 for p in range(0,5)])
    patterns=[[1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1],
              [1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1],
              [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0],
              [1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
              [1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1],
              [0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1],
              [0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0],
              [2, 2, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 2, 1, 1],
              [1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0],
              [0, 1, 0, 2, 1, 1, 2, 2, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1],
              [1, 1, 0, 0, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1],
              [0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 1, 1, 0, 1],
              [2, 2, 1, 1, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1],
              [2, 2, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 1, 1, 0, 1, 0, 1, 0, 2, 1, 1],
              [0, 1, 0, 2, 1, 1, 2, 2, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 1, 1, 0, 1],
              [1, 1, 0, 2, 0, 1, 2, 2, 1, 1, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1],
              [1, 1, 0, 1, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 1, 1, 0, 1, 0, 1, 0, 2, 1, 1, 2, 2, 0, 1, 0, 2],
              [2, 2, 1, 2, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 0, 1, 0, 2, 1, 2, 0, 2, 1, 2, 2, 2, 0, 2, 0, 2],
              [1, 2, 0, 2, 1, 2, 2, 2, 0, 2, 0, 2, 2, 2, 1, 2, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 0, 1, 0, 2],
              [1, 1, 0, 2, 0, 1, 2, 2, 0, 1, 0, 2, 1, 2, 0, 2, 1, 2, 2, 2, 0, 2, 0, 2, 2, 2, 1, 2, 0, 1],
              [2, 2, 0, 1, 0, 2, 1, 2, 0, 2, 1, 2, 2, 2, 0, 2, 0, 2, 2, 2, 1, 2, 0, 1, 1, 1, 0, 2, 0, 1],
              [2, 2, 0, 2, 0, 2, 2, 2, 1, 2, 0, 1, 1, 1, 0, 2, 0, 1, 2, 2, 0, 1, 0, 2, 1, 2, 0, 2, 1, 2]]

    return edge_list,pattern_converter,\
        raw_triangles,patterns
