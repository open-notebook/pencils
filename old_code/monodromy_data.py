'''Data for understanding quadric pencils on Fermat hypersurfaces in
CP^3 and quintic Z/5-Godeaux surfaces.

Each function returns:

edge_list

    a list of edge labels

patt_conv

    a function for colouring edges according to a pattern

raw_triangles

    a list of triples of oriented edges

patterns

    a list of patterns of geometric intersection numbers

When fed to the function curver_aux.make_pencil, this returns the
corresponding curver objects.

'''

def covered_relation(n):
    '''Returns the triangles, patterns, edge list
    and pattern_converter for the relation coming
    from the quadric pencil [x_1x_4:x_2x_3] on a
    Fermat surface \sum_{i=1}^4 x_i^n=0.

    We choose to represent the oriented arc k_{ij}
    in the ideal triangulation as (x,(i,j,k)) with
    x in {0,1} telling us the orientation
    (k in {0,...,5}, i,j in {0,...,n-1} specifying
    the arc)

    An oriented triangle is a triple of
    oriented edges.

    The curves we're interested in have the property
    that the geometric intersection with k_{ij} is
    independent of i,j, so our patterns are of length
    6 (telling us how curves hit 0_{ij},...,5_{ij}).
    In particular, the pattern_converter(i,j,k)=k.

    '''

    edge_list=[(i,j,k)
               for i in range(0,n)
               for j in range(0,n)
               for k in range(0,6)]
    pattern_converter=lambda tripl: tripl[2]
    
    A_tri=[[(1,(i,j,4)),(1,(i,j,1)),(0,(i,j,0))]
           for i in range(0,n)
           for j in range(0,n)]
    B_tri=[[(1,(i,(j+1)%n,0)),(1,(i,j,2)),(1,(i,j,3))]
           for i in range(0,n)
           for j in range(0,n)]
    C_tri=[[(0,(i,j,5)),(0,(i,j,2)),(0,(i,j,1))]
           for i in range(0,n)
           for j in range(0,n)]
    D_tri=[[(0,(i,j,4)),(0,(i,j,3)),(1,((i+1)%n,j,5))]
           for i in range(0,n)
           for j in range(0,n)]
    raw_triangles=A_tri+B_tri+C_tri+D_tri

    patterns=[[1,1,0,1,0,1],[1,0,1,0,1,1]]
    patterns.extend([[2*p,2*p+1,1,2*p+1,1,2*p]
                     for p in range(0,n)])

    return edge_list,pattern_converter,\
        raw_triangles,patterns

def z5_godeaux():
    '''Returns the triangles, patterns, edge list
    and pattern_converter for the relation coming
    from the bicanonical pencil on the Z/5-Godeaux
    surface obtained as the cyclic quotient of the
    Fermat quintic surface with weights (1,2,3,4).

    '''
    n=5
    edge_list=[(j,k)
               for j in range(0,n)
               for k in range(0,6)]
    pattern_converter=lambda duple: duple[1]
    gluing={1:4,4:2,2:0,0:3,3:1}
    g=lambda a: gluing[a]

    A_tri=[[(1,(j,4)),(1,(j,1)),(0,(j,0))]
           for j in range(0,n)]
    B_tri=[[(1,((j+1)%n,0)),(1,(j,2)),(1,(j,3))]
           for j in range(0,n)]
    C_tri=[[(0,(i,5)),(0,(i,2)),(0,(i,1))]
           for i in range(0,n)]
    D_tri=[[(0,(i,4)),(0,(i,3)),(1,(g(i),5))]
           for i in range(0,n)]
    raw_triangles=A_tri+B_tri+C_tri+D_tri
    
    patterns=[[1,1,0,1,0,1],[1,0,1,0,1,1]]
    patterns.extend([[2*p,2*p+1,1,2*p+1,1,2*p]
                     for p in range(0,n)])
    return edge_list,pattern_converter,\
        raw_triangles,patterns


