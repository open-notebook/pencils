#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from double_cover import *
from z5_godeaux_after_hurwitz import *
from itertools import combinations

'''
def find_droppable_curves(multicurves):
    connected_lifts=[len(multicurve.components())
                     for multicurve in multicurves]
    cntr=0
    droppable_curves=[]
    for i in range(0,len(multicurves)):
        if connected_lifts[i]==1:
            droppable_curves+=[cntr]
        cntr+=connected_lifts[i]
        
    return droppable_curves

def test_cover(branch_cut,S,s_curves):
    Z,z_curves,z_from_histlit,z_multi=make_pencil(*double_cover(branch_cut,
                                                                S,s_curves))
    droppable_curves=find_droppable_curves(z_multi)
    if len(droppable_curves)==8:
        for droppings in combinations(droppable_curves,4):
            curves=[z_curves[x]
                    for x in range(0,len(z_curves))
                    if x not in droppings]
            print(droppings,check_relation(Z,curves))
    elif len(droppable_curves)==12:
        print(check_relation(Z,z_curves))
'''
        
b_edges,b_patt_conv,b_raw_tri,b_patts=hurwitzed()
B,b_curves,b_from_hitlist,b_multi=make_pencil(b_edges,b_patt_conv,b_raw_tri,b_patts)
# Change the next line to get different covers
branch_cuts=[[0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0],
             [0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0],
             [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1],
             [0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
             [0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1],
             [0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1],
             [0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
             [0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0],
             [0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0],
             [0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 0],
             [0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1],
             [0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1],
             [0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
             [0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1]] #get them from godeaux_homology_calc

''' Already tried

[(0,0),(4,2),(4,5),(1,3),(1,2),(1,5),
 (3,4),(3,1),(3,5),(0,4)],
[(0,0),(0,1),(0,2),(1,0),(1,4),(1,3),
 (1,2),(1,1),(1,0),(0,2),(0,1),(0,0),
 (4,3),(4,4),(4,0),(3,2),(3,1),(3,4),
 (3,3),(4,0),(4,4),(4,3)],
[(0,0),(0,4),(0,3),(1,0),(1,4),(1,3),
 (1,2),(1,5),(3,4),(3,1),(3,2),(4,0),
 (4,1),(4,2)],
[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),
 (1,3),(1,4),(1,1),(1,5),(3,3),(3,2),
 (3,1),(3,4),(3,3),(4,0),(4,4),(4,3)],
[(0,0),(0,4),(0,3),(1,0),(1,4),(1,3),
 (1,2),(1,1),(1,0),(0,2),(0,1),(0,4),
 (3,5),(3,1),(3,4),(1,5),(1,2),(1,3),
 (4,5),(4,2),(4,3),(4,4),(4,0),(3,3),
 (3,4),(3,1),(3,2),(4,0),(4,1),(4,2)]
and got false for everything

'''

for branch_cut in branch_cuts:
    print("Testing ",branch_cut)
    test_cover(branch_cut,B,b_curves)
