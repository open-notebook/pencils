#!/usr/bin/python3

import curver
import curver_plus
import covering_spaces
import hurwitz_modified as hurwitz_moves


def do_hurwitz_moves_to_z5_godeaux(a,b,c,d,e):
    '''Given numbers a,b,c,d,e \in {1,2} this function picks a way to
    assign \sigma-orbits of vanishing cycles to critical points in the
    pencil after deformation, and performs the desired sequence of
    Hurwitz moves.

    The choice a,b,c,d,e=2,2,2,2,1 works; no other choice works.

    '''
    G,g_multi=covering_spaces.z5_godeaux()
    sigma=G.find_isometry(G,{0:~0})
    triplets=[split_multicurve(G,g_multi[i],sigma)
              for i in range(2,7)]
    
    initial_multi_labels={0 : 0,
                          'infty' : 1}
    initial_multi_labels.update({x : x+1 for x in range(1,16)})

    def other(x):
        if x==1:
            return 2
        elif x==2:
            return 1
    
    initial_multi_curves=[g_multi[0],g_multi[1],
                          triplets[0][0],triplets[0][a],triplets[0][other(a)],
                          triplets[1][b],triplets[1][other(b)],triplets[1][0],
                          triplets[2][c],triplets[2][0],triplets[2][other(c)],
                          triplets[3][d],triplets[3][other(d)],triplets[3][0],
                          triplets[4][e],triplets[4][other(e)],triplets[4][0]]
    move_lst=((4,15),(4,11),(4,9),(4,0),(4,8),(4,3),(4,6),(4,5),(4,7),(7,5),
              (6,7),(3,7),(3,6),(3,5),(3,4),(15,10),(15,12),(2,13),
              (13,1),(13,'infty'),
              (14,15),(13,15),(13,14),(10,11),(0,9),(0,10),(0,11),(0,12),
              (0,13),(0,14),(0,15))
    after_hurwitz=hurwitz_moves.hurwitz_moves(G,
                                              initial_multi_curves,
                                              initial_multi_labels,
                                              list(move_lst))
    after_curves=after_hurwitz[1]
    after_labels=after_hurwitz[2]

    claim1=after_curves[after_labels[4]]==after_curves[after_labels[10]]
    claim2=after_curves[after_labels[7]]==after_curves[after_labels[13]]
    return G,after_curves,claim1,claim2

def test_all_choices():
    for a in range(1,3):
        for b in range(1,3):
            for c in range(1,3):
                for d in range(1,3):
                    for e in range(1,3):
                        print(a,b,c,d,e)
                        print(do_hurwitz_moves_to_z5_godeaux(a,b,c,d,e)[2:4])


if __name__ == "__main__":
    print(do_hurwitz_moves_to_z5_godeaux(2,2,2,2,1)[2:4])


