'''Quick and dirty implementation of Gaussian elimination over Z/2Z.

Strongly relies on the fact that 1 is the only nonzero element of the field.'''

def gauss(A):
    '''Gaussian elimination for Z/2Z'''
    n = len(A)

    A=[[(A[i][j])%2
        for j in range(0,n+1)]
       for i in range(0,n)]
    
    for i in range(0, n):
        # Find first nonzero entry in column (below or including row i)
        col=[A[k][i] for k in range(i,n)]
        idx=next((p for p, x in enumerate(col) if x), None)
        if idx==None:
            raise Exception("Not invertible")
        entry=col[idx]
        
        # Swap with i^{th} row if needed
        if idx>0:
            A[i+idx],A[i]=A[i],A[i+idx]

        # Clear column
        for k in range(0,n):
            if k!=i:
                coeff=A[k][i]
                for j in range(0,n+1):
                    A[k][j]=(A[k][j]-coeff*A[i][j])%2

    return [A[i][n] for i in range(0,n)]

