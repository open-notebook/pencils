#!/usr/bin/python3

from curver_aux import *
from monodromy_data import *
from double_cover import *
from z5_godeaux_after_hurwitz import *
from gauss import *

def barlow_double_cover_ready_made():
    '''Returns the fibre and vanishing cycles for the bicanonical pencil
    on the surface Z which is the branched cover of the canonical
    model of the Barlow surface, branched at its four nodes.

    You don't need to call make_pencil: you just get the fibre and the
    vanishing cycles.

    '''
    # Start by setting up the Z/5-Godeaux and performing Hurwitz moves
    # to get the matching cycles for the nodal degeneration into
    # position
    T,curves,curve_from_hitlist,multi_curves=make_pencil(*hurwitzed())
    # Get a basis of closed curves for the Z/2-homology of the fibre
    # of the bicanonical pencil on the Z/5-Godeaux
    #branch_cut=[0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1,
    #            0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0]
    branch_cut=[1, 0, 0, 0, 1, 0,
                0, 0, 1, 1, 0, 1,
                0, 0, 0, 0, 0, 0,
                0, 1, 0, 0, 1, 1,
                0, 0, 1, 0, 0, 1]
    # Finally, we need to take the corresponding double cover of the
    # surface which is the fibre of the bicanonical on Z/5-Godeaux,
    # drop four of the vanishing cycles and check that the result is
    # still a relation in the mapping class group. When we pass to the
    # double cover, the curves other than curves[4,5,6,7,20,21,22,23]
    # lift to pairs of curves in the cover. The indexing therefore
    # means that curves[4,5,6,7,20,21,22,23] lift to curves
    # 8,9,10,11,36,37,38,39 in the cover. So we need to drop one from
    # each of the following pairs: {8,10}, {9,11}, {36,38},
    # {37,39}. Any choice will do.
    Z,z_curves,z_from_histlit,z_multi=make_pencil(*double_cover(branch_cut,
                                                                T,curves))
    lift_cpnts=[len(multicurve.components())
                for multicurve in z_multi]
    connected_lifts=[x for x in range(0,len(lift_cpnts))
                     if lift_cpnts[x]==1]
    droppings=[8,9,36,37]
    new_curves=[z_curves[x]
                for x in range(0,len(z_curves))
                if x not in droppings]
    tau_dict={1 : 27} # ?
    tau=Z.find_isometry(Z,tau_dict)
    return Z,new_curves,tau
